-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2017 at 03:10 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bhs_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `bhs_admin`
--

CREATE TABLE IF NOT EXISTS `bhs_admin` (
  `_id` int(11) NOT NULL,
  `bhs_username` varchar(100) NOT NULL,
  `bhs_password` varchar(100) NOT NULL,
  `_token` varchar(200) DEFAULT NULL,
  `_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bhs_users`
--

CREATE TABLE IF NOT EXISTS `bhs_users` (
  `_id` int(11) NOT NULL,
  `bhs_email` varchar(100) NOT NULL DEFAULT '',
  `bhs_password` varchar(200) DEFAULT NULL,
  `_token` varchar(200) DEFAULT NULL,
  `_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `bhs_users`
--
DELIMITER $$
CREATE TRIGGER `bhs_onDelete_stash_users` BEFORE DELETE ON `bhs_users`
 FOR EACH ROW INSERT INTO `bhs_users_stash`(`_event`, `_id`, `bhs_email`, `bhs_password`, `_token`, `_date_time`) VALUES ('onDelete', OLD._id, OLD.bhs_email, OLD.bhs_password, OLD._token, OLD._date_time)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `bhs_onUpdate_stash_users` BEFORE UPDATE ON `bhs_users`
 FOR EACH ROW INSERT INTO `bhs_users_stash`(`_event`, `_id`, `bhs_email`, `bhs_password`, `_token`, `_date_time`) VALUES ('onUpdate', OLD._id, OLD.bhs_email, OLD.bhs_password, OLD._token, OLD._date_time)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `bhs_users_stash`
--

CREATE TABLE IF NOT EXISTS `bhs_users_stash` (
  `_stash_id` int(11) NOT NULL,
  `_event` varchar(100) DEFAULT NULL,
  `_id` int(11) DEFAULT NULL,
  `bhs_email` varchar(100) DEFAULT NULL,
  `bhs_password` varchar(200) DEFAULT NULL,
  `_token` varchar(200) DEFAULT NULL,
  `_date_time` timestamp NULL DEFAULT NULL,
  `_stash_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bhs_users_stash`
--

INSERT INTO `bhs_users_stash` (`_stash_id`, `_event`, `_id`, `bhs_email`, `bhs_password`, `_token`, `_date_time`, `_stash_date_time`) VALUES
(4, 'onUpdate', 2, 'adeanladia0129@gmail.com', '1234', '1234', '2017-02-12 05:52:00', '2017-02-12 05:52:07'),
(5, 'onUpdate', 2, 'adeanladia0129@gmail.com', '12345678', '1234', '2017-02-12 05:52:00', '2017-02-12 05:52:30'),
(6, 'onDelete', 2, 'adeanladia0129@gmail.com', '12345678', '13563465', '2017-02-12 05:52:00', '2017-02-12 05:52:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bhs_admin`
--
ALTER TABLE `bhs_admin`
  ADD PRIMARY KEY (`bhs_username`),
  ADD UNIQUE KEY `_token` (`_token`),
  ADD KEY `_id` (`_id`);

--
-- Indexes for table `bhs_users`
--
ALTER TABLE `bhs_users`
  ADD PRIMARY KEY (`bhs_email`),
  ADD UNIQUE KEY `_token` (`_token`),
  ADD KEY `_id` (`_id`);

--
-- Indexes for table `bhs_users_stash`
--
ALTER TABLE `bhs_users_stash`
  ADD PRIMARY KEY (`_stash_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bhs_admin`
--
ALTER TABLE `bhs_admin`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bhs_users`
--
ALTER TABLE `bhs_users`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bhs_users_stash`
--
ALTER TABLE `bhs_users_stash`
  MODIFY `_stash_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
