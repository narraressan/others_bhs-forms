Vue.use(VueMaterial)

Vue.material.registerTheme("toolbar", { primary: { color: "grey", hue: 900 } });
Vue.material.registerTheme("default", { primary: "blue" });

const config = JSON.parse(conf);
const bhsadmin_global_server = config["protocol"] + "://" + config["address"] + ":" + config["port"];
// const bhsadmin_global_auth = "2012-0129-2012-0422";

var mainView = new Vue({
	el: "#app",
	data: {
		bhsadmin_var_snackbar: "",
		bhsadmin_var_email: "",
		bhsadmin_var_password: "",
		bhsadmin_var_email_toEdit: "",
		bhsadmin_var_password_toEdit: "",
		bhsadmin_var_id_toEdit: "",
		bhsadmin_var_userList: []
	},
	methods: {
		bhsadmin_func_logout: function(){
			NProgress.start();
			localStorage.clear();
			location.replace("/");
			NProgress.done();
		},
		bhsadmin_func_getUsers: function(){
			NProgress.start();
			$.ajax({
				method: "get",
				url: bhsadmin_global_server + "/bhs-admin-server/get-users"
			})
			.done(function(data, textStatus, xhr){
				// console.log(data);
				// console.log(textStatus);
				// console.log(xhr);

				mainView.bhsadmin_var_userList = data["data"];
				NProgress.done();
			});
		},
		bhsadmin_func_addUser: function(){
			NProgress.start();
			var userData = {
				email: this.bhsadmin_var_email,
				password: this.bhsadmin_var_password
			}

			if(this.bhsadmin_var_email.trim() != "" && this.bhsadmin_var_password.trim() != "" &&
				this.bhsadmin_var_email.length >= 8 && this.bhsadmin_var_password.length >= 8 &&
				!(/\s/).test(this.bhsadmin_var_email) && !(/\s/).test(this.bhsadmin_var_password)){
				
				$.ajax({
					method: "post",
					url: bhsadmin_global_server + "/bhs-admin-server/create-user",
					headers: { "Authorization": JSON.parse(localStorage.getItem("admin"))["_token"]},
					data: userData
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					mainView.bhsadmin_var_snackbar = data["message"];
					mainView.$refs.snackbar.open();
					NProgress.done();

					if(data["data"] == 200){ location.reload(); }
				});
			}
			else{
				this.bhsadmin_var_snackbar = "Sorry, there was an error with your email/password."
				this.$refs.snackbar.open();
				NProgress.done();
			}
		},
		bhsadmin_func_loadUser: function(nth){
			NProgress.start();
			this.bhsadmin_var_email_toEdit =  nth.bhs_email;
			this.bhsadmin_var_password_toEdit = nth.bhs_password;
			this.bhsadmin_var_id_toEdit = nth._id;
			console.log(nth._id + " -- " + this.bhsadmin_var_password + " " + this.bhsadmin_var_email);
			NProgress.done();
		},
		bhsadmin_func_unloadUser: function(nth){
			NProgress.start();
			this.bhsadmin_var_email_toEdit =  "";
			this.bhsadmin_var_password_toEdit = "";
			this.bhsadmin_var_id_toEdit = "";
			NProgress.done();
		},
		bhsadmin_func_editUser: function(){
			NProgress.start();
			var userData = {
				email: this.bhsadmin_var_email_toEdit,
				password: this.bhsadmin_var_password_toEdit,
				_id: this.bhsadmin_var_id_toEdit
			}

			if(this.bhsadmin_var_email_toEdit.trim() != "" && this.bhsadmin_var_password_toEdit.trim() != "" &&
				this.bhsadmin_var_email_toEdit.length >= 8 && this.bhsadmin_var_password_toEdit.length >= 8 &&
				!(/\s/).test(this.bhsadmin_var_email_toEdit) && !(/\s/).test(this.bhsadmin_var_password_toEdit)){

				$.ajax({
					method: "post",
					url: bhsadmin_global_server + "/bhs-admin-server/update-user",
					headers: { "Authorization": JSON.parse(localStorage.getItem("admin"))["_token"]},
					data: userData
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					mainView.bhsadmin_var_snackbar = data["message"];
					mainView.$refs.snackbar.open();
					NProgress.done();

					if(data["data"] == 200){ 
						mainView.bhsadmin_func_getUsers(); 
						mainView.$refs.editUser.close();
						mainView.bhsadmin_func_unloadUser();
					}
				});

			}
			else{
				this.bhsadmin_var_snackbar = "Sorry, there was an error with your email/password."
				this.$refs.snackbar.open();
				NProgress.done();
			}
		},
		bhsadmin_func_deleteUser: function(nth){
			NProgress.start();
			var userData = {
				_id: nth._id
			}

			$.ajax({
				method: "post",
				url: bhsadmin_global_server + "/bhs-admin-server/delete-user",
				headers: { "Authorization": JSON.parse(localStorage.getItem("admin"))["_token"]},
				data: userData
			})
			.done(function(data, textStatus, xhr){
				// console.log(data);
				// console.log(textStatus);
				// console.log(xhr);

				mainView.bhsadmin_var_snackbar = data["message"];
				mainView.$refs.snackbar.open();
				NProgress.done();

				if(data["data"] == 200){ mainView.bhsadmin_func_getUsers(); }
			});
		}
	},
	mounted: function(){
		this.bhsadmin_func_getUsers();
	}
});