Vue.use(VueMaterial)

Vue.material.registerTheme("default", {
	primary: "blue"
});

const config = JSON.parse(conf);
const bhsadmin_global_server = config["protocol"] + "://" + config["address"] + ":" + config["port"];
// const bhsadmin_global_auth = "2012-0129-2012-0422";

var mainView = new Vue({
	el: "#app",
	data: {
		bhsadmin_var_username: "",
		bhsadmin_var_password: "",
		bhsadmin_var_snackbar: "Hi!",
		bhsadmin_var_auth: ""
	},
	methods: {
		bhsadmin_func_signup: function(){
			// console.log(this.bhsadmin_var_username + " " + this.bhsadmin_var_password);
			NProgress.start();

			var userData = {
				username: this.bhsadmin_var_username,
				password: this.bhsadmin_var_password
			}

			if(this.bhsadmin_var_username.trim() != "" && this.bhsadmin_var_password.trim() != "" &&
				this.bhsadmin_var_username.length >= 8 && this.bhsadmin_var_password.length >= 8 &&
				!(/\s/).test(this.bhsadmin_var_username) && !(/\s/).test(this.bhsadmin_var_password)){
				
				$.ajax({
					method: "post",
					url: bhsadmin_global_server + "/bhs-admin-server/create-admin",
					headers: { "Authorization": this.bhsadmin_var_auth},
					data: userData
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					mainView.bhsadmin_var_snackbar = data["message"];
					mainView.$refs.snackbar.open();
					NProgress.done();
				});

			}
			else{
				this.bhsadmin_var_snackbar = "Sorry, there was an error with your username/password."
				this.$refs.snackbar.open();
				NProgress.done();
			}

			this.$refs.authConfirm.close();
		},
		bhsadmin_func_login: function(){
			// console.log(this.bhsadmin_var_username + " " + this.bhsadmin_var_password);
			NProgress.start();

			var userData = {
				username: this.bhsadmin_var_username,
				password: this.bhsadmin_var_password
			}

			if(this.bhsadmin_var_username.trim() != "" && this.bhsadmin_var_password.trim() != "" &&
				this.bhsadmin_var_username.length >= 8 && this.bhsadmin_var_password.length >= 8 &&
				!(/\s/).test(this.bhsadmin_var_username) && !(/\s/).test(this.bhsadmin_var_password)){
				
				$.ajax({
					method: "post",
					url: bhsadmin_global_server + "/bhs-admin-server/login-admin",
					data: userData
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["data"] != null){
						localStorage.setItem("admin", JSON.stringify(data["data"][0]));
						location.replace("/admin");
					}

					mainView.bhsadmin_var_snackbar = data["message"];
					mainView.$refs.snackbar.open();
					NProgress.done();
				});

			}
			else{
				this.bhsadmin_var_snackbar = "Sorry, there was an error with your username/password."
				this.$refs.snackbar.open();
			}

			this.$refs.authConfirm.close();
		}
	}
})