// usage: node bhs-admin-server.js
// usage: nodemon bhs-admin-server.js

// import utilities
var app = require("koa")();
var route = require('koa-router')();
var kbody = require('koa-body')();

// create default variables


// default functions
function *getMessage(){
	console.log(this.request);
	console.log(this.request.header);
	console.log(this.request.method);
	console.log(this.request.href);
	console.log(this.request.path);
	console.log(this.request.query);

	console.log(this.response.header);
	console.log(this.response.status);
	console.log(this.response.message);
	console.log(this.response.body);
	console.log(this.response.type);
	console.log(this.response.set("body", "hahahahhahha"));


	this.body = this.response;
};

function *postMessage(){
	this.body = JSON.stringify(this.request.body);
};

function *allMessage(){
	this.body = "all messages";
}

function *sendID(){
    this.body = 'id: ' + this.params.id + ' and name: ' + this.params.name;
}


// RESTfull api endpoints
route.get('/hello', getMessage);
route.post('/post', kbody, postMessage);
route.all('/test', allMessage);
route.get('/things/:name/:id', sendID);
app.use(route.routes());

app.listen(3000, function(){
	console.log("Server running on port 3000")
});