// usage: nodemon bhs-admin-server.js
// usage: node bhs-admin-server.js

// linux
// usage: node bhs-admin-server.js



// import utilities
const reqExpress = require("express"); 
const reqIPFilter = require("express-ipfilter").IpFilter;
const reqbodyParser = require("body-parser");
const reqPath = require("path");
const reqMySQL = require("mysql");
const reqFs = require("fs");
const reqUtil = require("util");
const crypto = require("crypto");



// custom console.log function to write text file for monitoring
	var logName = "admin-backend-logs.txt";
	var logFile = reqFs.createWriteStream(logName, { flags: "a" });
	var logStdout = process.stdout;
	console.log = function () {
		logFile.write(reqUtil.format.apply(null, arguments) + "\n");
		logStdout.write(reqUtil.format.apply(null, arguments) + "\n");
	}
	console.error = console.log;
// --------------------------------------------------------



// default values
	var mysql_connect = reqMySQL.createConnection({ host: "localhost", port: "3306", user: "nto2017", password: "LionCity", database: "bhs_admin"});
	var express = reqExpress();
	var config = {
		"default-port": 5000,
		"blacklisted": [],
		"en-algorithm": "aes-256-ctr",
		"en-key": "2012-0129-2012-0422"
	};
	// express.use(reqIPFilter(config["blacklisted"])); ---> use this when you intend to block an IP
	express.use(reqbodyParser.urlencoded({ extended: false }));

	// default admin fron-end page
	express.use("/frontend", reqExpress.static(reqPath.join(__dirname, "/../frontend")));
	express.get("/", (req, res) => { res.sendFile(reqPath.join(__dirname + "/../frontend/index.html")); });
	express.get("/admin", (req, res) => { res.sendFile(reqPath.join(__dirname + "/../frontend/admin-dashboard.html")); });
// --------------------------------------------------------



// default functions
	
	// encryption and decryption source: http://lollyrock.com/articles/nodejs-encryption/

	function encrypt(text){
		var cipher = crypto.createCipher(config["en-algorithm"], config["en-key"]);
		var crypted = cipher.update(text, "utf8", "hex");
		crypted += cipher.final("hex");
		return crypted;
	}

	function decrypt(text){
		var decipher = crypto.createDecipher(config["en-algorithm"], config["en-key"]);
		var dec = decipher.update(text, "hex", "utf8");
		dec += decipher.final("utf8");
		return dec;
	}

	function rand(){ return Math.random().toString(36).substr(2); };

	function validateAuth(auth, reqData, responseData, res, successCallback, errorCallback){
		// console.log(auth + " " + reqData + " " + responseData + " " + res + " " + successCallback + " " + errorCallback);
		// get data from mysql
		mysql_connect.query("SELECT `_token` FROM `bhs_admin` WHERE BINARY `_token`=?", [auth], function (error, results, fields) {
			if(error){ 
				console.log(error);
				errorCallback();
			}
			else{
				if(results.length == 1){ successCallback(); }
				else{ errorCallback(); }
			}
		});
	}
// --------------------------------------------------------



// RESTful endpoints
	express.get("/bhs-admin-server/check-activity", (req, res) => {
		res.send("Admin server active - " + new Date());
	});

	// create new admin account
	express.post("/bhs-admin-server/create-admin", (req, res) => {
		var responseData = { "message": null, "data": null };

		var auth = req.header("Authorization");
		var reqData = req.body;

		if(auth == config["en-key"]){
			var token = rand() + rand();

			mysql_connect.query("INSERT INTO `bhs_admin`(`bhs_username`, `bhs_password`, `_token`) VALUES (?, ?, ?)", [reqData["username"], encrypt(reqData["password"]), token], function (error, results, fields) {
				if(error){ 
					console.log(error);
					responseData["message"] = "Error creating new Admin. It seems this username already exists.";
				}
				else{ responseData["message"] = "New Admin successfully created."; }

				res.send(responseData);
			});
		}
		else{
			responseData["message"] = "Oops! Invalid authentication key."; 
			res.send(responseData);
		}
	});

	// login admin account
	express.post("/bhs-admin-server/login-admin", (req, res) => {
		var responseData = { "message": null, "data": null };

		var reqData = req.body;

		// get data from mysql
		mysql_connect.query("SELECT `bhs_username`, `_token` FROM `bhs_admin` WHERE BINARY `bhs_username`=? and `bhs_password`=?", [reqData["username"], encrypt(reqData["password"])], function (error, results, fields) {
			if(error){ 
				console.log(error);
				responseData["message"] = "Sorry, I don't know you."; 
			}
			else{
				if(results.length == 1){ 
					responseData["message"] = "Admin credentials verified.";
					responseData["data"] = results; 
				}
				else if(results.length > 1){ 
					console.log("multiple results: " + reqData["username"] + " | " + encrypt(reqData["password"]));
					responseData["message"] = "I am detecting an illegal access. Reporting issue to admin..."; 
				}
				else{
					// no result
					responseData["message"] = "Sorry, but I don't know you."; 
				}
			}

			res.send(responseData);
		});
	});

	// create new user
	express.post("/bhs-admin-server/create-user", (req, res) => {
		var responseData = { "message": null, "data": null };

		var auth = req.header("Authorization");
		var reqData = req.body;

		var successCallback = function(){
			var token = rand() + rand();

			mysql_connect.query("INSERT INTO `bhs_users`(`bhs_email`, `bhs_password`, `_token`) VALUES (?, ?, ?)", [reqData["email"], encrypt(reqData["password"]), token], function (error, results, fields) {
				if(error){ 
					console.log(error);
					responseData["message"] = "Error creating new user. It seems this email already exists.";
				}
				else{ 
					responseData["message"] = "New user successfully created."; 
					responseData["data"] = 200;
				}

				res.send(responseData);
			});
		}

		var errorCallback = function(){
			responseData["message"] = "Oops! Invalid authentication key."; 
			res.send(responseData);
		}

		validateAuth(auth, reqData, responseData, res, successCallback, errorCallback);
	});

	// edit user
	express.post("/bhs-admin-server/update-user", (req, res) => {
		var responseData = { "message": null, "data": null };

		var auth = req.header("Authorization");
		var reqData = req.body;

		var successCallback = function(){
			mysql_connect.query("UPDATE `bhs_users` set `bhs_email`=? WHERE `_id`=?", [reqData["email"], reqData["_id"]], function (error, results, fields) {
				if(error){ 
					console.log(error);
					responseData["message"] = "Error updating this user. It seems this email already exists.";
				}
				else{ 
					responseData["message"] = "User successfully updated."; 
					responseData["data"] = 200;
				}

				res.send(responseData);
			});
		}

		var errorCallback = function(){
			responseData["message"] = "Oops! Invalid authentication key."; 
			res.send(responseData);
		}

		validateAuth(auth, reqData, responseData, res, successCallback, errorCallback);
	});

	// delete user
	express.post("/bhs-admin-server/delete-user", (req, res) => {
		var responseData = { "message": null, "data": null };

		var auth = req.header("Authorization");
		var reqData = req.body;

		var successCallback = function(){
			mysql_connect.query("DELETE FROM `bhs_users` WHERE `_id`=?", [reqData["_id"]], function (error, results, fields) {
				if(error){ 
					console.log(error);
					responseData["message"] = "Unable to remove this user. He's very persistent!";
				}
				else{ 
					responseData["message"] = "User has been removed."; 
					responseData["data"] = 200;
				}

				res.send(responseData);
			});
		}
		
		var errorCallback = function(){
			responseData["message"] = "Oops! Invalid authentication key."; 
			res.send(responseData);
		}

		validateAuth(auth, reqData, responseData, res, successCallback, errorCallback);
	});

	// get user list
	express.get("/bhs-admin-server/get-users", (req, res) => {
		var responseData = { "message": null, "data": null };

		// get data from mysql
		mysql_connect.query("SELECT `_id`, `bhs_email`, `bhs_password`, `_date_time` FROM `bhs_users` order by `_date_time`", function (error, results, fields) {
			if(error){ 
				console.log(error);
				responseData["message"] = "Unable to get user list."; 
			}
			else{
				responseData["message"] = "Gathered all users.";
				responseData["data"] = results;
			}

			res.send(responseData);
		});
	});
// --------------------------------------------------------



// Proper error handling
	express.use(function(req, res){ res.status(400).sendFile(reqPath.join(__dirname + "/../frontend/404.html"));});
	express.use(function(error, req, res, next) { 
		console.log(error);
		res.status(500).sendFile(reqPath.join(__dirname + "/../frontend/500.html"));
	});
// --------------------------------------------------------




// default instance
	express.listen(config["default-port"], () => { 
		console.log("listening to port " + config["default-port"]);
		mysql_connect.connect(function(err){
			if(err){ console.error("error connecting: " + err.stack);}
			else{ console.log("connected as id " + mysql_connect.threadId);}
		});
	});
// --------------------------------------------------------