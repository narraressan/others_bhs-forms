<!-- USAGE -->
- This folder contains all the forms (HTML) which are pre-made by the developer - @narra
- All forms have specific functions and fields specified by the clien, but all their submit buttons returns exactly the same JSON format
- All data are saved and view-able only to the specific form it was made for.

<!-- resourse -->
- use "milligram" (http://milligram.github.io/) for simplicity

<!-- load format -->
<!-- this is not used for now, since the forms are pre-made -->
[
	...
	{
		section-id: [random section token],
		label: [this is another html with a <label> for this section title],
		note: [this is another html with a <small> for this section desciption],
		elements: [
			{ 
				dom-id: [this is important, this will be used in the request response upon submission]
				dom: [html content - this can be a set of input fields where some are empty/disabled etc. so when a formula is triggered it will only navigate through "SIBLING" and "CHILD" elements], 
				default-value: [default value of dom],
				dom-formula: [name of formula applied for dom]
			},
			...
		],
	}
	...
]

<!-- return format -->
[
	...
	{
		dom-id: [id of the input field],
		field-alias: [this will be used to load data in the builder 2.0],
		answer: [value of input by the user]
	}
	...
]

<!-- LIMITATION -->
* due to complex calcuations, all forms are pre and custom made to support each functionalities desired by the client
