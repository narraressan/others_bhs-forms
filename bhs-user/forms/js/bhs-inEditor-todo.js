// default vars
	const config = JSON.parse(conf);
	const bhsuser_global_server = config["protocol"] + "://" + config["address"] + ":" + config["port"];
	var stagedForm = localStorage.getItem("stagedBuilder");


// load html template
	function loadTemplate(stagedForm){
		var temp = JSON.parse(stagedForm);

		document.getElementById("formName").innerHTML = temp["title"];
		document.getElementById("formCreated").innerHTML = temp["time"];
		document.getElementById("formRespondent").innerHTML = temp["respondent"];

		try{
			var form = JSON.parse(temp["form_content"]["formatted"]);
			// disable all css and js if any - it may disrupt all other functions from main form-loader.html
			document.getElementById("form-layout").innerHTML = form["html"];
			document.getElementById("form-layout").innerHTML += "<hr><button class=\"btn btn-lg btn-primary pull-right\" type=\"button\" onclick=\"collectInputs();\">Save <span class=\"glyphicon glyphicon-send\"></span></button>";
			$("#form-layout").find("link").remove();
			$("#form-layout").find("script").remove();
		}
		catch(e){ 
			alert("Form did not load properly. Please try again. Thank you."); 
			returnHome();
		}

		// load answers if there are already pre-contents
		try{ distributeInputs(JSON.parse(temp["formatted"])); }
		catch(e){ 
			// most likely - there is no answers recorded...
		}
	};
	loadTemplate(stagedForm);
	

// collect all input
	function collectInputs(){
		var formData = [];

		var temp = $('#form-layout').find('input, textarea, select');
		for (var i = 0; i < temp.length; i++) {
			var key = temp[i].id;

			formData[i] = {
				"id": temp[i].id,
				"value": null,
				"type": temp[i].type,
			} 
			if(temp[i].type == "checkbox" || temp[i].type == "radio"){ formData[i]["value"] = temp[i].checked; }
			else{ formData[i]["value"] = temp[i].value; }
		}

		// send data through REST
			var formatted = formData;
			var rawHTML = $("#form-layout").serialize();

			// set data for sending
				var stash = JSON.parse(localStorage.getItem("stagedBuilder"));
				var commit = {
					raw: rawHTML,
					formatted: formatted
				};

			// -------------------------------------------
				$.ajax({
					method: "post",
					url: bhsuser_global_server + "/bhs-lab-server/user-updateResponse",
					contentType: "application/json",
					headers: { "Authorization": JSON.parse(localStorage.getItem("bhs-user"))["_token"] },
					data: JSON.stringify({ stash: stash, commit: commit })
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					alert(data["message"]);
					if(data["data"] == 200){
						stash["raw"] = JSON.stringify(rawHTML);
						stash["formatted"] = JSON.stringify(formatted);
						localStorage.setItem("stagedBuilder", JSON.stringify(stash));
						// console.log(JSON.parse(localStorage.getItem("stagedBuilder")));
					}
				});
	}

	function distributeInputs(inputs){
		var noId = 0;
		for (var i = 0; i < inputs.length; i++) {
			// execute if field has id
			if(inputs[i]["id"].trim() != ""){
				var doc = document.getElementById(inputs[i]["id"]);
				if(doc.type == inputs[i]["type"]){ 
					if(doc.type == "checkbox" || doc.type == "radio"){ doc.checked = inputs[i]["value"]; }
					else{ doc.value = inputs[i]["value"]; }
				}
			}
			else { noId++; }
		}

		if(noId > 0){ document.getElementById("others").innerHTML += "<br>Oops! There are " + noId + " fields w/o unique IDs, I am unable to load their contents correctly."; }
	}


// identfy all input elements if clicked
	$("#form-layout").find("input, textarea, select").on("click", function(){
		var tag = this.tagName;
		var type = this.type;
		var id = this.id;
		var todo = this.getAttribute("todo");

		if(id == "" || id == "undefined"){ id = "<span class=\"alert-danger\">* no id found</span>" }

		document.getElementById("tag").innerHTML = "<small>Tag:</small> " + tag;
		document.getElementById("type").innerHTML = "<small>Type:</small> " + type;
		document.getElementById("todo").innerHTML = "<small>To Do:</small> " + todo;
		document.getElementById("id").innerHTML = "<small>ID:</small> " + id;
	});


// update all elements with todo
	$("#form-layout").find("input[todo*=':'], textarea[todo*=':'], select[todo*=':']").each(function(){
		var todo = this.getAttribute("todo").split(":");
		$(this).on(todo[0].replace("on", ""), function(){
			if(todo[1] == "colonyCount_pcm(this);"){ colonyCount_pcm(this); }
		});
	});


// list of all pre-defined todo functions
	function colonyCount_pcm(el){
		console.log("Solve!" + el.value);
	}


// default page unload functions
	function returnHome(){
		localStorage.removeItem("stagedBuilder");
		window.location.replace("/bhs-lab");
	};
	$(window).bind("beforeunload", function(event) {
		return "Make sure you have saved your changes";
	});