
// default data
const config = JSON.parse(conf);
const bhsuser_global_server = config["protocol"] + "://" + config["address"] + ":" + config["port"];



// vue plugins
Vue.use(VueMaterial);
Vue.material.registerTheme("tabsResp", { primary: { color: "cyan", hue: 900 } });
Vue.material.registerTheme("tabsForm", { primary: { color: "teal", hue: 600 } });
// Vue.material.registerTheme("tabs", { primary: "blue", warn: "white", accent: { color: "grey", hue: 800 } });
Vue.material.registerTheme("default", { primary: "blue" });



// Dashboard: vue instance
var bhsdashboard = new Vue({
	el: "#bhsdashboard",
	data: {
		// variable factory
		bhsuser_var_snackbar: "Hi",
		bhsuser_var_builderStaged: null,
		// user variables
		bhsuser_var_user: {
			email: "",
			newPassword: null,
			newFormName: null,
			toggleTimeline: null,
			toggleCircle: null,
			myForms: [],
			myResponses: [],
			myHistory: [],
			myCircle: []
		},
		// view renders
		bhsuser_var_view: {
			welcome_home: false,
			builder: false,
			response: false,
			// this "forms" refers to the pdf templates
			forms: false
		},
		bhsuser_stage_triggered: {
			form_fillup: false,
			form_builder: false
		},
		// SHARE/UNSHARE variables
		bhsuser_var_sharedStaged: {
			arr: [],
			form_title: null,
			form_id: null 
		},
		// DELETE Form variable
		bhsuser_var_deleteStagedForm: {
			form_title: null,
			form_id: null
		},
		// DELETE response variable
		bhsuser_var_deleteStagedResponse: {
			response_form: null,
			response_id: null
		},
		// others
		// filter_forms_sample: [],
		bhsuser_var_formSearch: "",
		bhsuser_var_responseSearch: "",
		bhsuser_var_circleSearch: "",
		bhsuser_var_historyFilter: null,
		bhsuser_var_form_counters: {
			form_total: 0,
			form_authored: 0,
			form_shared: 0
		},
		bhsuser_var_response_counters: {
			response_total: 0,
			response_sent: 0,
			response_received: 0
		}
	},
	methods: {
		// FACTORY functions --- VIEW RENDERING
			bhsuser_func_setDefault: function(){
				NProgress.start();
				
				// set default view
				var viewsTemp = JSON.parse(localStorage.getItem("views"));
				this.bhsuser_var_user["toggleTimeline"] = viewsTemp["timelineView"];
				this.bhsuser_var_user["toggleCircle"] = viewsTemp["circleView"];
				this.bhsuser_func_changeView(viewsTemp["currView"]);
				localStorage.setItem("views", JSON.stringify(viewsTemp));

				// set default user
				this.bhsuser_var_user["email"] = JSON.parse(localStorage.getItem("bhs-user"))["bhs_email"].split("@")[0];
				this.bhsuser_var_user["password"] = JSON.parse(localStorage.getItem("bhs-user"))["bhs_password"];

				NProgress.done();
			},
			bhsuser_func_changeView: function(view){
				NProgress.start();
				for(var nth in this.bhsuser_var_view){ this.bhsuser_var_view[nth] = false; }
				this.bhsuser_var_view[view] = true;

				var viewsTemp = JSON.parse(localStorage.getItem("views"));
				viewsTemp["currView"] = view;
				localStorage.setItem("views", JSON.stringify(viewsTemp));

				if(this.bhsuser_var_view["builder"]){
					if(localStorage.getItem("stagedBuilder") != null){ this.bhsuser_var_builderStaged = JSON.parse(localStorage.getItem("stagedBuilder")); }
					
					// Always allow popups (tab is considered a popup)
					// window.open("/bhs-builder", "_blank");
				}

				NProgress.done();
			},
			bhsuser_func_autoFocus: function(){
				var viewsTemp = JSON.parse(localStorage.getItem("views"));
				viewsTemp["timelineView"] = this.bhsuser_var_user["toggleTimeline"];
				viewsTemp["circleView"] = this.bhsuser_var_user["toggleCircle"];
				localStorage.setItem("views", JSON.stringify(viewsTemp));

				if(document.getElementById("submissionId") != null){ document.getElementById("submissionId").focus(); }
				if(document.getElementById("formsId") != null){ document.getElementById("formsId").focus(); }
			},
			bhsuser_func_activeStagedLink: function(key){
				this.bhsuser_stage_triggered["form_fillup"] = false;
				this.bhsuser_stage_triggered["form_builder"] = false;

				this.bhsuser_stage_triggered[key] = true;
			},
		// FACTORY functions --- ATTENDANCE
			bhsuser_func_getAttendance: function(){
				// NOTE:
				// no pagination implemented

				NProgress.start();
				$.ajax({
					method: "get",
					url: bhsuser_global_server + "/bhs-lab-server/user-getAllStatus",
					data: { author_id: JSON.parse(localStorage.getItem("bhs-user"))["_token"] }
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["data"] == null){
						bhsdashboard.bhsuser_var_snackbar = data["message"];
						bhsdashboard.$refs.snackbar.open();
					}
					else{ 
						// remove my own attendance from view
						var tempCircle = data["data"];
						for (var i = 0; i < tempCircle.length; i++) {
							if(tempCircle[i]["token"] == JSON.parse(localStorage.getItem("bhs-user"))["_token"]){ tempCircle.splice(i, 1); }
						}
						bhsdashboard.bhsuser_var_user["myCircle"] = tempCircle;
					}
					
					NProgress.done();
				});
			},
		// FACTORY functions --- HISTORY
			bhsuser_func_getHistory: function(){
				// NOTE:
				// no pagination implemented

				NProgress.start();
				$.ajax({
					method: "get",
					url: bhsuser_global_server + "/bhs-lab-server/user-getAllActHistory",
					contentType: "application/json",
					headers: { "Authorization": JSON.parse(localStorage.getItem("bhs-user"))["_token"] },
					data: { author_id: JSON.parse(localStorage.getItem("bhs-user"))["_token"] }
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["data"] == null){
						bhsdashboard.bhsuser_var_snackbar = data["message"];
						bhsdashboard.$refs.snackbar.open();
					}
					else{ bhsdashboard.bhsuser_var_user["myHistory"] = data["data"]; }
					
					NProgress.done();
				});
			},
			bhsuser_func_historyStatus: function(eventName){
				if(eventName == "onShareForm"){ return "shared the form"; }
				if(eventName == "onDeleteForm"){ return "deleted the form"; }
				if(eventName == "onCreateForm"){ return "created"; }
				if(eventName == "onUpdateForm"){ return "edited the form"; }
				if(eventName == "onUnshareForm"){ return "unshared the form"; }
				if(eventName == "onDownloadForm"){ return "downloaded a copy of"; }
				if(eventName == "onResponse"){ return "submitted a response on form"; }
				if(eventName == "onUpdateResponse"){ return "updated a response on form"; }
				if(eventName == "onDeleteResponse"){ return "deleted a response on form"; }
				if(eventName == "onDownloadResponse"){ return "downloaded a response for"; }

				return "";
			},
			bhsuser_func_historyEmail: function(email){
				try{ 
					if(email == JSON.parse(localStorage.getItem("bhs-user"))["bhs_email"]){ return "You"; }
					return email.split("@")[0];
				}
				catch(e){ return ""; }
			},
			bhsuser_func_historyCount: function(myHistory){
				try{ return myHistory.length; }
				catch(e){ return "No"; }
			},
			bhsuser_func_historyStart: function(myHistory){
				try{ return myHistory[myHistory.length - 1]["time"]; }
				catch(e){ return ""; }
			},
		// FACTORY functions --- FORMS ----> pdf form templates
			bhsuser_func_getForms: function(form_id, download){
				// NOTE:
				// no pagination implemented

				NProgress.start();
				$.ajax({
					method: "get",
					url: bhsuser_global_server + "/bhs-lab-server/user-getAllForms",
					contentType: "application/json",
					headers: { "Authorization": JSON.parse(localStorage.getItem("bhs-user"))["_token"] },
					data: { form_id: form_id }
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["data"] == null){
						bhsdashboard.bhsuser_var_snackbar = data["message"];
						bhsdashboard.$refs.snackbar.open();
					}
					else{ 
						if(form_id == null){ 
							bhsdashboard.bhsuser_var_form_counters["form_total"] = 0;
							bhsdashboard.bhsuser_var_form_counters["form_authored"] = 0;
							bhsdashboard.bhsuser_var_form_counters["form_shared"] = 0;

							bhsdashboard.bhsuser_var_user["myForms"] = data["data"]; 
							bhsdashboard.bhsuser_var_form_counters["form_total"] = bhsdashboard.bhsuser_var_user["myForms"].length;

							var authTemp = JSON.parse(localStorage.getItem("bhs-user"))["_token"];
							for (var i = 0; i < data["data"].length; i++) {
								// count: forms authored
								if(data["data"][i]["author"] == authTemp){ bhsdashboard.bhsuser_var_form_counters["form_authored"]++; }

								// count: forms shared
								if($.inArray(authTemp, JSON.parse(data["data"][i]["shared"])) == 1){ bhsdashboard.bhsuser_var_form_counters["form_shared"]++; }
							}
						}
						else if(download){ 
							// download function here!
							alert("download form " + form_id);
							console.log(data["data"]);
						}
						else{ 
							bhsdashboard.bhsuser_func_changeView("builder");
							bhsdashboard.bhsuser_var_builderStaged = data["data"][0];
							localStorage.setItem("stagedBuilder", JSON.stringify(bhsdashboard.bhsuser_var_builderStaged));
						}
					}
					
					NProgress.done();
				});
			},
			bhsuser_func_createForm: function(){
				NProgress.start();
				$.ajax({
					method: "post",
					url: bhsuser_global_server + "/bhs-lab-server/user-createNewForm",
					contentType: "application/json",
					headers: { "Authorization": JSON.parse(localStorage.getItem("bhs-user"))["_token"] },
					data: JSON.stringify({ form_name: this.bhsuser_var_user["newFormName"] })
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["data"] != null){
						bhsdashboard.bhsuser_var_user["newFormName"] = "";
						if(document.getElementById("newFormName")){ document.getElementById("newFormName").focus(); }

						bhsdashboard.bhsuser_func_getHistory();
						bhsdashboard.bhsuser_func_getForms(data["data"], false);
					}				

					bhsdashboard.bhsuser_var_snackbar = data["message"];
					bhsdashboard.$refs.snackbar.open();
					NProgress.done();
				});
			},
			bhsuser_func_stageShared: function(form){
				this.bhsuser_var_sharedStaged["arr"] = JSON.parse(form["shared"]);
				this.bhsuser_var_sharedStaged["form_title"] = form["title"];
				this.bhsuser_var_sharedStaged["form_id"] = form["token"];
			},
			bhsuser_func_shareUnshare: function(){
				this.bhsuser_var_sharedStaged["arr"].push(JSON.parse(localStorage.getItem("bhs-user"))["_token"]);

				NProgress.start();
				$.ajax({
					method: "post",
					url: bhsuser_global_server + "/bhs-lab-server/user-shareUnshareForm",
					contentType: "application/json",
					headers: { "Authorization": JSON.parse(localStorage.getItem("bhs-user"))["_token"] },
					data: JSON.stringify(this.bhsuser_var_sharedStaged)
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["data"] == 200){
						bhsdashboard.bhsuser_var_sharedStaged = {
							arr: [],
							form_title: null,
							form_id: null 
						};
						bhsdashboard.bhsuser_func_getHistory();
					}

					bhsdashboard.bhsuser_var_snackbar = data["message"];
					bhsdashboard.$refs.snackbar.open();
					NProgress.done();
				});
			},
			bhsuser_func_stageDeleteForm: function(form){
				this.bhsuser_var_deleteStagedForm["form_id"] = form["token"];
				this.bhsuser_var_deleteStagedForm["form_title"] = form["title"];
			},
			bhsuser_func_deleteForm: function(){
				NProgress.start();
				$.ajax({
					method: "post",
					url: bhsuser_global_server + "/bhs-lab-server/user-deleteForm",
					contentType: "application/json",
					headers: { "Authorization": JSON.parse(localStorage.getItem("bhs-user"))["_token"] },
					data: JSON.stringify({ form_id: this.bhsuser_var_deleteStagedForm["form_id"] })
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["data"] == 200){
						bhsdashboard.bhsuser_var_deleteStagedForm = {
							form_title: null,
							form_id: null
						};
						bhsdashboard.bhsuser_func_getForms(null, false);
						bhsdashboard.bhsuser_func_getHistory();
					}

					bhsdashboard.bhsuser_var_snackbar = data["message"];
					bhsdashboard.$refs.snackbar.open();
					NProgress.done();
				});
			},
		// FACTORY functions --- SUBMISSIONS
			bhsuser_func_getSubmission: function(response_id, download){
				// NOTE:
				// no pagination implemented

				NProgress.start();
				$.ajax({
					method: "get",
					url: bhsuser_global_server + "/bhs-lab-server/user-getAllSubmissions",
					contentType: "application/json",
					headers: { "Authorization": JSON.parse(localStorage.getItem("bhs-user"))["_token"] },
					data: { response_id: response_id }
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["data"] == null){
						bhsdashboard.bhsuser_var_snackbar = data["message"];
						bhsdashboard.$refs.snackbar.open();
					}
					else{ 
						if(response_id == null){ 
							bhsdashboard.bhsuser_var_response_counters["response_total"] = 0;
							bhsdashboard.bhsuser_var_response_counters["response_sent"] = 0;
							bhsdashboard.bhsuser_var_response_counters["response_received"] = 0;

							bhsdashboard.bhsuser_var_user["myResponses"] = data["data"]; 
							bhsdashboard.bhsuser_var_response_counters["response_total"] = bhsdashboard.bhsuser_var_user["myResponses"].length;

							var authTemp = JSON.parse(localStorage.getItem("bhs-user"))["bhs_email"];
							for (var i = 0; i < data["data"].length; i++) {
								// count: forms authored
								if(data["data"][i]["respondent"] == authTemp){ bhsdashboard.bhsuser_var_response_counters["response_sent"]++; }
								else { bhsdashboard.bhsuser_var_response_counters["response_received"]++; }
							}
						}
						else if(download){ 
							// download function here!
							alert("download response! " + response_id);
							console.log(data["data"]);
						}
						else{ 
							// use ajax as a promise
							$.ajax(bhsdashboard.bhsuser_func_getForms(data["data"][0]["form"], false)).then(function(){ 
								var temp = bhsdashboard.bhsuser_var_builderStaged;
								bhsdashboard.bhsuser_var_builderStaged = data["data"][0];
								bhsdashboard.bhsuser_var_builderStaged["form_content"] = temp
								
								localStorage.setItem("stagedBuilder", JSON.stringify(bhsdashboard.bhsuser_var_builderStaged));
							});
						}
					}
					
					NProgress.done();
				});
			},
			bhsuser_func_createSubmission: function(form_id){
				NProgress.start();
				$.ajax({
					method: "post",
					url: bhsuser_global_server + "/bhs-lab-server/user-createResponseOnForm",
					contentType: "application/json",
					headers: { "Authorization": JSON.parse(localStorage.getItem("bhs-user"))["_token"] },
					data: JSON.stringify({ form_id: form_id })
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["data"] != null){
						bhsdashboard.bhsuser_func_getHistory();
						bhsdashboard.bhsuser_func_getSubmission(data["data"], false);
					}				

					bhsdashboard.bhsuser_var_snackbar = data["message"];
					bhsdashboard.$refs.snackbar.open();
					NProgress.done();
				});
			},
			bhsuser_func_stageDeleteResponse: function(response){
				this.bhsuser_var_deleteStagedResponse["response_form"] = response["title"];
				this.bhsuser_var_deleteStagedResponse["response_form_id"] = response["form"];
				this.bhsuser_var_deleteStagedResponse["response_id"] = response["response_id"];
			},
			bhsuser_func_deleteResponse: function(){
				NProgress.start();
				$.ajax({
					method: "post",
					url: bhsuser_global_server + "/bhs-lab-server/user-deleteResponse",
					contentType: "application/json",
					headers: { "Authorization": JSON.parse(localStorage.getItem("bhs-user"))["_token"] },
					data: JSON.stringify({ 
						response_id: this.bhsuser_var_deleteStagedResponse["response_id"],
						form_id: this.bhsuser_var_deleteStagedResponse["response_form_id"],
					})
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["data"] == 200){
						bhsdashboard.bhsuser_var_deleteStagedResponse = {
							response_form: null,
							response_id: null
						};
						bhsdashboard.bhsuser_func_getSubmission(null, false);
						bhsdashboard.bhsuser_func_getHistory();
					}

					bhsdashboard.bhsuser_var_snackbar = data["message"];
					bhsdashboard.$refs.snackbar.open();
					NProgress.done();
				});
			},
		// FACTORY functions --- CHANGE PASSWORD
			bhsuser_func_changePassword: function(){
				NProgress.start();

				var userData = { new_pass: this.bhsuser_var_user["newPassword"] };

				if(this.bhsuser_var_user["newPassword"].trim() != "" && this.bhsuser_var_user["newPassword"].length >= 8 && !(/\s/).test(this.bhsuser_var_user["newPassword"])){
					$.ajax({
						method: "post",
						url: bhsuser_global_server + "/bhs-lab-server/update-user-data",
						headers: { "Authorization": JSON.parse(localStorage.getItem("bhs-user"))["_token"]},
						data: userData
					})
					.done(function(data, textStatus, xhr){
						// console.log(data);
						// console.log(textStatus);
						// console.log(xhr);

						if(data["data"] != null){ bhsdashboard.bhsuser_func_logout(); }

						bhsdashboard.bhsuser_var_snackbar = data["message"];
						bhsdashboard.$refs.snackbar.open();
						NProgress.done();
					});
				}
				else{
					this.bhsuser_var_snackbar = "Sorry, there was an error with your new password."
					this.$refs.snackbar.open();
					NProgress.done();
				}
			},
		// FACTORY functions --- LOGOUT
			bhsuser_func_logout: function(){
				NProgress.start();
				$.ajax({
					method: "post",
					url: bhsuser_global_server + "/bhs-lab-server/user-logout",
					headers: { "Authorization": JSON.parse(localStorage.getItem("bhs-user"))["_token"]},
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["data"] == 200){
						localStorage.clear();
						location.replace("/");
					}

					bhsdashboard.bhsuser_var_snackbar = data["message"];
					bhsdashboard.$refs.snackbar.open();
					NProgress.done();
				});
			}
	},
	computed:{
		bhsuser_computed_filteredHistory: function(){
			var temp = [];
			if(this.bhsuser_var_historyFilter == null || this.bhsuser_var_historyFilter == "" || this.bhsuser_var_historyFilter == "all"){
				temp = this.bhsuser_var_user['myHistory'];
			}
			else if(this.bhsuser_var_historyFilter == "submissions"){				
				for (var i = 0; i < this.bhsuser_var_user['myHistory'].length; i++) {
					if(this.bhsuser_var_user['myHistory'][i]["status"] == "onResponse"){ temp.push(this.bhsuser_var_user['myHistory'][i]); }
				}			
			}
			else if(this.bhsuser_var_historyFilter == "updates"){
				for (var i = 0; i < this.bhsuser_var_user['myHistory'].length; i++) {
					if(this.bhsuser_var_user['myHistory'][i]["status"] == "onUpdateResponse"){ temp.push(this.bhsuser_var_user['myHistory'][i]); }
					if(this.bhsuser_var_user['myHistory'][i]["status"] == "onShareForm"){ temp.push(this.bhsuser_var_user['myHistory'][i]); }
					if(this.bhsuser_var_user['myHistory'][i]["status"] == "onUpdateForm"){ temp.push(this.bhsuser_var_user['myHistory'][i]); }
				}
			}
			// else if(this.bhsuser_var_historyFilter == "deletes"){
			// 	for (var i = 0; i < this.bhsuser_var_user['myHistory'].length; i++) {
			// 		if(this.bhsuser_var_user['myHistory'][i]["status"] == "onDeleteResponse"){ temp.push(this.bhsuser_var_user['myHistory'][i]); }
			// 		if(this.bhsuser_var_user['myHistory'][i]["status"] == "onDeleteForm"){ temp.push(this.bhsuser_var_user['myHistory'][i]); }
			// 	}
			// }

			// filter by form
			else{ console.log(this.bhsuser_var_historyFilter); }

			return temp
		},
		bhsuser_computed_searchSubmissions: function(){
			var temp = [];

			if(this.bhsuser_var_responseSearch == ""){ return this.bhsuser_var_user["myResponses"]; }
			for (var i = 0; i < this.bhsuser_var_user["myResponses"].length; i++) {
				if(this.bhsuser_var_user["myResponses"][i]["title"].toLowerCase().indexOf(this.bhsuser_var_responseSearch.toLowerCase()) + 1 ){ temp.push(this.bhsuser_var_user["myResponses"][i]); }
				else if(this.bhsuser_var_user["myResponses"][i]["time"].toLowerCase().indexOf(this.bhsuser_var_responseSearch.toLowerCase()) + 1 ){ temp.push(this.bhsuser_var_user["myResponses"][i]); }
				else if(this.bhsuser_var_user["myResponses"][i]["respondent"].toLowerCase().indexOf(this.bhsuser_var_responseSearch.toLowerCase()) + 1 ){ temp.push(this.bhsuser_var_user["myResponses"][i]); }
			}
			return temp;
		},
		bhsuser_computed_searchForm: function(){
			var temp = [];
			if(this.bhsuser_var_formSearch == ""){ return this.bhsuser_var_user["myForms"]; }
			for (var i = 0; i < this.bhsuser_var_user["myForms"].length; i++) {
				if(this.bhsuser_var_user["myForms"][i]["title"].toLowerCase().indexOf(this.bhsuser_var_formSearch.toLowerCase()) + 1 ){ temp.push(this.bhsuser_var_user["myForms"][i]); }
				else if(this.bhsuser_var_user["myForms"][i]["time"].toLowerCase().indexOf(this.bhsuser_var_formSearch.toLowerCase()) + 1 ){ temp.push(this.bhsuser_var_user["myForms"][i]); }
			}
			return temp;
		},
		bhsuser_computed_searchCircle: function(){
			var temp = [];
			if(this.bhsuser_var_circleSearch == ""){ return this.bhsuser_var_user['myCircle']; }
			for (var i = 0; i < this.bhsuser_var_user["myCircle"].length; i++) {
				if(this.bhsuser_var_user["myCircle"][i]["email"].toLowerCase().indexOf(this.bhsuser_var_circleSearch.toLowerCase()) + 1 ){ temp.push(this.bhsuser_var_user["myCircle"][i]); }
			}
			return temp;
		}
	},
	mounted: function(){
		this.bhsuser_func_setDefault();
		
		// load basic data from db
		this.bhsuser_func_getAttendance();
		this.bhsuser_func_getHistory();

		if(this.bhsuser_var_view["forms"]){ this.bhsuser_func_getForms(null, false); }
		if(this.bhsuser_var_view["response"]){ this.bhsuser_func_getSubmission(null, false); }
		if(this.bhsuser_var_view["builder"]){ this.bhsuser_func_changeView("welcome_home"); }
	}
})