
// default data
const config = JSON.parse(conf);
const bhsuser_global_server = config["protocol"] + "://" + config["address"] + ":" + config["port"];



// vue plugins
Vue.use(VueMaterial);
Vue.material.registerTheme("toolbar", { primary: { color: "grey", hue: 900 } });
Vue.material.registerTheme("default", { primary: "blue" });



// login: vue instance
var bhslogin = new Vue({
	el: "#bhslogin",
	data: {
		bhsuser_var_email: "",
		bhsuser_var_password: "",
		bhsuser_var_snackbar: "Hi!",
	},
	methods: {
		bhsuser_func_login: function(){
			// console.log(this.bhsuser_var_email + " " + this.bhsuser_var_password);
			NProgress.start();

			var userData = {
				email: this.bhsuser_var_email,
				password: this.bhsuser_var_password
			}

			if(this.bhsuser_var_email.trim() != "" && this.bhsuser_var_password.trim() != "" &&
				this.bhsuser_var_email.length >= 8 && this.bhsuser_var_password.length >= 8 &&
				!(/\s/).test(this.bhsuser_var_email) && !(/\s/).test(this.bhsuser_var_password)){
				
				$.ajax({
					method: "post",
					url: bhsuser_global_server + "/bhs-lab-server/user-login",
					data: userData
				})
				.done(function(data, textStatus, xhr){
					console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["data"] != null){
						localStorage.setItem("bhs-user", JSON.stringify(data["data"][0]));
						localStorage.setItem("views", JSON.stringify({ currView: "welcome_home", timelineView: true, circleView: true }));
						location.replace("/bhs-lab");
					}

					bhslogin.bhsuser_var_snackbar = data["message"];
					bhslogin.$refs.snackbar.open();
					NProgress.done();
				});

			}
			else{
				this.bhsuser_var_snackbar = "Sorry, there was an error with your username/password."
				this.$refs.snackbar.open();
			}
		}
	}
})