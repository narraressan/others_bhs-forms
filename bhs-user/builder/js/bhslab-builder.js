
// default data
const config = JSON.parse(conf);
const bhsuser_global_server = config["protocol"] + "://" + config["address"] + ":" + config["port"];



// LIST of COMPONENTS
// all elements inside id="bhsComponent-editable" are editable by user in html mode
// CURRENTLY NO SUPPORT FOR CUSTOM MADE COMPONENTS
	const bhsbuilderComponents = {
		"InputField": {
			"grid-width": 3,
			"grid-height": 2,
			"html": '<div class="grid-stack-item-content bhslab-content form-group"><br><h5 id="bhsComponent-editable">[TEXT]</h5><input class="form-control" type="text"></div>',
			"description": "Input field that accepts all input forms (e.g. number, email, text)."
		},
		"TextArea": {
			"grid-width": 4,
			"grid-height": 3,
			"html": '<div class="grid-stack-item-content bhslab-content form-group"><h5 id="bhsComponent-editable">[TEXT]</h5><textarea class="form-control" rows="5"></textarea></div>',
			"description": "Text area caters multiple lines of texts."
		},
		"DatePicker": {
			"grid-width": 3,
			"grid-height": 2,
			"html": '<div class="grid-stack-item-content bhslab-content form-group"><br><h5 id="bhsComponent-editable">[DATE]</h5><input class="form-control" type="date"></div>',
			"description": "Input field that accepts date."
		},
		"InsertImg": {
			"grid-width": 3,
			"grid-height": 3,
			"html": '<div class="grid-stack-item-content bhslab-content"><span id="bhsComponent-editable"><img class="img-fluid" src="http://placehold.it/350x150"></span></div>',
			"description": "Upload image and display in this panel."
		},
		"InsertLink": {
			"grid-width": 4,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><span id="bhsComponent-editable"><a href="#LINK">[LINK]</a></span></div>',
			"description": "Insert link and display as text.",
		},
		"TextLabel": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><p id="bhsComponent-editable">[TEXT]</p></div>',
			"description": "Display long or short message/texts."
		},

		// do not support this component yet...
		// "TextList": {
		// 	"grid-width": 3,
		// 	"grid-height": 2,
		// 	"html": '<div class="grid-stack-item-content bhslab-content"><ul id="bhsComponent-editable"><li>[LIST]</li><li>[LIST]</li><li>[LIST]</li></ul></div>',
		// 	"description": "Display list of texts or points in a list form."
		// },

		"Divider": {
			"grid-width": 12,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><div style="height: 50%;border-bottom: 3px solid grey" id="bhsComponent-editable"></div></div>',
			"description": "Section divider."
		},
		"Checkbox": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><br><label class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"><span class="custom-control-indicator"></span><span class="custom-control-description" id="bhsComponent-editable">[Checkbox Label]</span></label></div>',
			"description": "Show checkbox options."
		},
		"Container": {
			"grid-width": 4,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><span id="bhsComponent-editable"></span></div>',
			"description": "Empty container for spacing"
		},

		// Advance components TextLabel - LEFT
		"advance-TextLabel-h1-left": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h1 id="bhsComponent-editable">[TEXT]</h1></div>',
			"description": "Display long or short message/texts as Huge texts (h1)."
		},
		"advance-TextLabel-h2-left": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h2 id="bhsComponent-editable">[TEXT]</h2></div>',
			"description": "Display long or short message/texts as Huge texts (h2)."
		},
		"advance-TextLabel-h3-left": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h3 id="bhsComponent-editable">[TEXT]</h3></div>',
			"description": "Display long or short message/texts as Huge texts (h3)."
		},
		"advance-TextLabel-h4-left": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h4 id="bhsComponent-editable">[TEXT]</h4></div>',
			"description": "Display long or short message/texts as Huge texts (h4)."
		},
		"advance-TextLabel-h5-left": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h5 id="bhsComponent-editable">[TEXT]</h5></div>',
			"description": "Display long or short message/texts as Huge texts (h5)."
		},
		"advance-TextLabel-h6-left": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h6 id="bhsComponent-editable">[TEXT]</h6></div>',
			"description": "Display long or short message/texts as Huge texts (h6)."
		},
		"advance-TextLabel-small-left": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><small id="bhsComponent-editable">[TEXT]</small></div>',
			"description": "Display long or short message/texts as small texts (small)."
		},
		"advance-TextLabel-b-left": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><b id="bhsComponent-editable">[TEXT]</b></div>',
			"description": "Display long or short message/texts as bold texts (b)."
		},
		"advance-TextLabel-i-left": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><i id="bhsComponent-editable">[TEXT]</i></div>',
			"description": "Display long or short message/texts as italic texts (i)."
		},
		"advance-TextLabel-u-left": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><u id="bhsComponent-editable">[TEXT]</u></div>',
			"description": "Display long or short message/texts as underlined texts (u)."
		},

		// Advance components TextLabel - CENTER
		"advance-TextLabel-h1-center": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h1 id="bhsComponent-editable" class="text-center">[TEXT]</h1></div>',
			"description": "Display long or short message/texts as Huge texts (h1) - CENTER."
		},
		"advance-TextLabel-h2-center": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h2 id="bhsComponent-editable" class="text-center">[TEXT]</h2></div>',
			"description": "Display long or short message/texts as Huge texts (h2) - CENTER."
		},
		"advance-TextLabel-h3-center": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h3 id="bhsComponent-editable" class="text-center">[TEXT]</h3></div>',
			"description": "Display long or short message/texts as Huge texts (h3) - CENTER."
		},
		"advance-TextLabel-h4-center": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h4 id="bhsComponent-editable" class="text-center">[TEXT]</h4></div>',
			"description": "Display long or short message/texts as Huge texts (h4) - CENTER."
		},
		"advance-TextLabel-h5-center": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h5 id="bhsComponent-editable" class="text-center">[TEXT]</h5></div>',
			"description": "Display long or short message/texts as Huge texts (h5) - CENTER."
		},
		"advance-TextLabel-h6-center": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h6 id="bhsComponent-editable" class="text-center">[TEXT]</h6></div>',
			"description": "Display long or short message/texts as Huge texts (h6) - CENTER."
		},
		"advance-TextLabel-small-center": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content text-center"><small id="bhsComponent-editable">[TEXT]</small></div>',
			"description": "Display long or short message/texts as small texts (small) - CENTER."
		},
		"advance-TextLabel-b-center": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content text-center"><b id="bhsComponent-editable">[TEXT]</b></div>',
			"description": "Display long or short message/texts as bold texts (b) - CENTER."
		},
		"advance-TextLabel-i-center": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content text-center"><i id="bhsComponent-editable">[TEXT]</i></div>',
			"description": "Display long or short message/texts as italic texts (i) - CENTER."
		},
		"advance-TextLabel-u-center": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content text-center"><u id="bhsComponent-editable">[TEXT]</u></div>',
			"description": "Display long or short message/texts as underlined texts (u) - CENTER."
		},

		// Advance components TextLabel - RIGHT
		"advance-TextLabel-h1-right": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h1 id="bhsComponent-editable" class="text-right">[TEXT]</h1></div>',
			"description": "Display long or short message/texts as Huge texts (h1) - RIGHT."
		},
		"advance-TextLabel-h2-right": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h2 id="bhsComponent-editable" class="text-right">[TEXT]</h2></div>',
			"description": "Display long or short message/texts as Huge texts (h2) - RIGHT."
		},
		"advance-TextLabel-h3-right": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h3 id="bhsComponent-editable" class="text-right">[TEXT]</h3></div>',
			"description": "Display long or short message/texts as Huge texts (h3) - RIGHT."
		},
		"advance-TextLabel-h4-right": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h4 id="bhsComponent-editable" class="text-right">[TEXT]</h4></div>',
			"description": "Display long or short message/texts as Huge texts (h4) - RIGHT."
		},
		"advance-TextLabel-h5-right": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h5 id="bhsComponent-editable" class="text-right">[TEXT]</h5></div>',
			"description": "Display long or short message/texts as Huge texts (h5) - RIGHT."
		},
		"advance-TextLabel-h6-right": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><h6 id="bhsComponent-editable" class="text-right">[TEXT]</h6></div>',
			"description": "Display long or short message/texts as Huge texts (h6) - RIGHT."
		},
		"advance-TextLabel-small-right": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content text-right"><small id="bhsComponent-editable">[TEXT]</small></div>',
			"description": "Display long or short message/texts as small texts (small) - RIGHT."
		},
		"advance-TextLabel-b-right": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content text-right"><b id="bhsComponent-editable">[TEXT]</b></div>',
			"description": "Display long or short message/texts as bold texts (b) - RIGHT."
		},
		"advance-TextLabel-i-right": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content text-right"><i id="bhsComponent-editable">[TEXT]</i></div>',
			"description": "Display long or short message/texts as italic texts (i) - RIGHT."
		},
		"advance-TextLabel-u-right": {
			"grid-width": 3,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content text-right"><u id="bhsComponent-editable">[TEXT]</u></div>',
			"description": "Display long or short message/texts as underlined texts (u) - RIGHT."
		},

		// Advance components TextList - number
		// do not support this component yet...
		// "advance-TextList-ol": {
		// 	"grid-width": 3,
		// 	"grid-height": 2,
		// 	"html": '<div class="grid-stack-item-content bhslab-content"><ol id="bhsComponent-editable"><li>[LIST]</li><li>[LIST]</li><li>[LIST]</li></ol></div>',
		// 	"description": "Display list of texts or points in a list form."
		// },

		// Advance components Radio button - NOT SUPPORTED
		// ------------------

		// drop down select option
		"advance-dropDown": {
			"grid-width": 2,
			"grid-height": 1,
			"html": '<div class="grid-stack-item-content bhslab-content"><select class="custom-select" id="bhsComponent-editable"><option selected>Open this select menu</option><option value="1">[One]</option><option value="2">[Two]</option><option value="3">[Three]</option></select></div>',
			"description": "Display list of texts or points in a list form."
		},

		// input types links & images
		"advance-InputField-link": {
			"grid-width": 3,
			"grid-height": 2,
			"html": '<div class="grid-stack-item-content bhslab-content form-group"><br><h5 id="bhsComponent-editable">[LINK]</h5><input class="form-control" type="text" id="linkInput"></div>',
			"description": "Input field that accepts text and render as link (e.g. number, email, text)."
		},
		"advance-InputField-img": {
			"grid-width": 3,
			"grid-height": 2,
			"html": '<div class="grid-stack-item-content bhslab-content form-group"><br><h5 id="bhsComponent-editable">[IMAGE]</h5><input class="form-control" type="text" id="imgInput"></div>',
			"description": "Input field that accepts text and render as image."
		},
		"advance-InputField-formula": {
			"grid-width": 3,
			"grid-height": 2,
			"html": '<div class="grid-stack-item-content bhslab-content form-group"><br><h5 id="bhsComponent-editable">[FORMULA]</h5><input class="form-control" type="text" id="formulaInput"></div>',
			"description": "Input field that accepts formula and render as result."
		},
		"advance-InputField-formulaColonyCount-AS": {
			"grid-width": 3,
			"grid-height": 2,
			"html": '<div class="grid-stack-item-content bhslab-content form-group"><br><h5 id="bhsComponent-editable">[COLONY COUNT - AIR SAMPLE]</h5><input class="form-control" type="text" id="formulaInputColonyCount-AS" placeholder="# of observed colonies on plate"></div>',
			"description": "Input field that accepts formula and render as result."
		},
		"advance-InputField-formulaCFU-cm-squared": {
			"grid-width": 3,
			"grid-height": 2,
			"html": '<div class="grid-stack-item-content bhslab-content form-group"><br><h5>[CFU/cm<sup>2</sup>]</h5><input class="form-control" type="text" id="formulaInputCFU-cm-squared" placeholder="CFU per cm squared" disabled></div>',
			"description": "Input field that accepts formula and render as result."
		}
	};

// template data
	var bhsbuilderRaw = null;
	var bhsbuilderFormatted = null;
	var formToken = null;
	var responseToken = null;
	var compCount = 0;

	var currentComponent = null;
	var currentMode = null;

	var options = {
		removable: ".trash",
		removeTimeout: 100,
		verticalMargin: 0,
		resizable: { handles: "e, se, s, sw, w" },
		alwaysShowResizeHandle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
	};

	// init gridstack on load and every refresh
	function initGridStack(gridConfig){ $(".grid-stack").gridstack(gridConfig); }
	initGridStack(options);



	// set default mode to "Edit" onload and every refresh
	function setMode(mode){ 
		// set check during refresh or first load
		$("#"+mode).prop("checked", true);
		currentComponent = null;
		currentMode = mode;

		// preview and every modes that is not Edit
		if(mode != "Edit"){
			$(".grid-stack").data("gridstack").setStatic(true);
			$(".grid-stack-item-content").removeClass("bhslab-content");
			$(".grid-stack-item-content").removeClass("bhslab-content-lastClicked");
		}

		if(mode == "Edit"){
			$(".grid-stack").data("gridstack").setStatic(false);
			$(".grid-stack-item-content").addClass("bhslab-content");
		}
		else if(mode == "Response"){ 
			// MAKE SURE YOU HAVE SAVED YOUR FILES BEFORE GOING OUT OF RESPONSE MODE - it will be loast
			console.log("all input forms are now submittable");

			// traverse to all elements
			var responses = JSON.parse(JSON.parse(localStorage.getItem("stagedBuilder"))["formatted"]);

			for (var i = 0; i < responses.length; i++) {
				try{
					// find the first input field only
					if($($("#" + responses[i]["id"] + " :input")[0]).is(":checkbox") == false){
						// remove all 'toDeleteThisElement' id
						$("#toDeleteThisElement").remove();

						// do not use show() - it adds a style attribute so it wont match the key
						$($("#" + responses[i]["id"] + " :input")[0]).removeAttr("style");
						// console.log(responses[i]["input"][$("#" + responses[i]["id"] + " :input")[0].outerHTML]);
					}
				}
				catch(err){ 
					// no input field located
					console.log("no element for id " + responses[i]["id"]);
				}

				$('.grid-stack').data('gridstack').resize(
					$('.grid-stack-item')[i],
					$($('.grid-stack-item')[i]).attr('data-gs-width'),
					Math.ceil(($('.grid-stack-item-content')[i].scrollHeight + $('.grid-stack').data('gridstack').opts.verticalMargin) / ($('.grid-stack').data('gridstack').cellHeight() + $('.grid-stack').data('gridstack').opts.verticalMargin))
				);
			}
		}
		else if(mode == "Publish"){ 
			loadPDF_file();
			console.log("hide elements that are empty, this is the view for download");
		}
	}



	// open PDF - Report
	function loadPDF_file(){
		var responseId = JSON.parse(localStorage.getItem("stagedBuilder"))["response_id"];
		window.open("/bhs-pdf/?res=" + responseId).focus();
	}



	// save components functions
	function saveComponents(){

		if(currentMode == "Edit"){
			// only able to get node elements, NOT INPUT VALUES
			var rawHTML = $(".grid-stack").html();
			var formatted = _.map($('.grid-stack .grid-stack-item:visible'), function (el) {
				el = $(el);
				// do not store node --- too much characters
				var node = el.data('_gridstack_node');
				return {
					id: el.attr('data-custom-id'),
					x: node.x,
					y: node.y,
					"grid-width": node.width,
					"grid-height": node.height,
					html: node.el[0].firstChild.outerHTML,
					"description": null
				};
			});

			if(formToken != null && formToken != ""){ 
				var stash = JSON.parse(localStorage.getItem("stagedBuilder"));
				var commit = JSON.parse(localStorage.getItem("stagedBuilder"));

				commit["title"] = $("#formTitle").val();
				commit["raw"] = rawHTML;
				commit["formatted"] = JSON.stringify(formatted);
				
				// -------------------------------------------
				$.ajax({
					method: "post",
					url: bhsuser_global_server + "/bhs-lab-server/user-updateForm",
					contentType: "application/json",
					headers: { "Authorization": JSON.parse(localStorage.getItem("bhs-user"))["_token"] },
					data: JSON.stringify({ stash: stash, commit: commit })
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					alert(data["message"]);
					if(data["data"] == 200){
						localStorage.setItem("stagedBuilder", JSON.stringify(commit));
						// console.log(JSON.parse(localStorage.getItem("stagedBuilder")));
					}
				});
			}
		}
		else if(currentMode == "Response"){ 
			if(responseToken != null && responseToken != ""){ 
				var rawHTML = $(".grid-stack").html();
				var formatted = _.map($(".grid-stack .grid-stack-item:visible"), function (el) {
					el = $(el);
					// do not store node --- too much characters
					var node = el.data("_gridstack_node");
					var input = {};

					try{ 
						// there sould only be ONE input per component
						// get all input fields and values anyway, later load only the first one.
						var temp = $("#" + el.attr("data-custom-id") + " :input");
						for (var i = 0; i < temp.length; i++) {
							if($(temp[i]).is(":checkbox")){ input[temp[i].outerHTML] = temp[i].checked; }
							// else if($(temp[i]).is("select")){ input[temp[i].outerHTML] = temp[i].value; }
							else{ input[temp[i].outerHTML] = temp[i].value; }

							// console.log(input[temp[i].outerHTML]);
						}
					}
					catch(err){ console.log("There is no input field in this component"); }
					
					return {
						id: el.attr("data-custom-id"),
						html: node.el[0].firstChild.outerHTML,
						input: input
					};
				});

				var stash = JSON.parse(localStorage.getItem("stagedBuilder"));
				var commit = {
					raw: rawHTML,
					formatted: formatted
				};
				
				// -------------------------------------------
				$.ajax({
					method: "post",
					url: bhsuser_global_server + "/bhs-lab-server/user-updateResponse",
					contentType: "application/json",
					headers: { "Authorization": JSON.parse(localStorage.getItem("bhs-user"))["_token"] },
					data: JSON.stringify({ stash: stash, commit: commit })
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					alert(data["message"]);
					if(data["data"] == 200){
						stash["raw"] = JSON.stringify(rawHTML);
						stash["formatted"] = JSON.stringify(formatted);
						localStorage.setItem("stagedBuilder", JSON.stringify(stash));
						// console.log(JSON.parse(localStorage.getItem("stagedBuilder")));
					}
				});
			}
		}
		else if(currentMode == "Preview"){ 
			alert("Save is not supported in this mode. Move to Edit mode.");
			// console.log("NO unique saving method required.");
		}
		else if(currentMode == "Publish"){ 
			alert("Unable to save in this State. Go back to Response mode and then save.");
		}

		// console.log(JSON.stringify(formatted));
		// console.log(formatted);
	}



	// load components from db query
	// form_id in this URL | token in localstorage
	function loadComponents(thisFormatted){
		try{
			var formatted = thisFormatted;
			var grid = $(".grid-stack").data("gridstack");

			for (var i = 0; i < formatted.length; i++) {
				compCount++;

				var uniqueId = formatted[i]["id"];
				var x = formatted[i]["x"];
				var y = formatted[i]["y"];
				var w = formatted[i]["grid-width"];
				var h = formatted[i]["grid-height"];
				var html = formatted[i]["html"];

				grid.addWidget($("<div data-custom-id=\""+ uniqueId +"\" id=\""+ uniqueId +"\">"+ html +"</div>"), x, y, w, h);
			}
		}
		catch(err) { 
			// variable formatted is most likely empty and cannot be parsed.
			console.log("variable is empty.");
		}

		// add event when selected
		$(".grid-stack-item-content").click(function() {
			if(currentMode == "Edit"){
				$(".grid-stack-item-content").removeClass("bhslab-content-lastClicked");
				$(this).addClass("bhslab-content-lastClicked");

				currentComponent = this;
			}
		});

		if(responseToken != null && responseToken != ""){ 
			var responses = JSON.parse(JSON.parse(localStorage.getItem("stagedBuilder"))["formatted"]);

			for (var i = 0; i < responses.length; i++) {
				try{
					// find the first input field only
					if($($("#" + responses[i]["id"] + " :input")[0]).is(":checkbox")){
						$("#" + responses[i]["id"] + " :input")[0].checked = responses[i]["input"][$("#" + responses[i]["id"] + " :input")[0].outerHTML];
					}
					else{ $("#" + responses[i]["id"] + " :input")[0].value = responses[i]["input"][$("#" + responses[i]["id"] + " :input")[0].outerHTML]; }
				}
				catch(err){ 
					// no input field located
					console.log("no element for id " + responses[i]["id"]);
				}
			}
		}
	}



// EDIT mode ----------------------------------
	// add single builder component
	function addComponent(component){ 
		// allow CRUD on edit mode only
		if(currentMode != "Edit"){ return; }

		compCount++;
		var uniqueId = component + "-" + formToken + "-" + responseToken + "-" + compCount + "-" + Date.now();
		var w = bhsbuilderComponents[component]["grid-width"];
		var h = bhsbuilderComponents[component]["grid-height"];

		var grid = $(".grid-stack").data("gridstack");
		grid.addWidget($("<div data-custom-id=\""+ uniqueId +"\" id=\""+ uniqueId +"\">"+ bhsbuilderComponents[component]["html"] +"</div>"), null, null, w, h, true);

		$(".grid-stack-item-content").click(function() {
			if(currentMode == "Edit"){
				$(".grid-stack-item-content").removeClass("bhslab-content-lastClicked");
				$(this).addClass("bhslab-content-lastClicked");

				currentComponent = this;
			}
		});
	}


	function pinComponent(){ 
		// allow CRUD on edit mode only
		if(currentMode != "Edit"){ return; }

		try{ 
			var component = currentComponent.parentNode;
			var grid = $(".grid-stack").data("gridstack");
			grid.movable(component, false);
			grid.resizable(component, false);
			grid.locked(component, true);
		}
		catch(e){ console.log("No node selected."); }
	}


	function unpinComponent(){ 
		// allow CRUD on edit mode only
		if(currentMode != "Edit"){ return; }

		try{
			var component = currentComponent.parentNode;
			var grid = $(".grid-stack").data("gridstack");
			grid.movable(component, true);
			grid.resizable(component, true);
			grid.locked(component, false);
		}
		catch(e){ console.log("No node selected."); }
	}


	function removeComponent(){ 
		// allow CRUD on edit mode only
		if(currentMode != "Edit"){ return; }

		try{ $(".grid-stack").data("gridstack").removeWidget(currentComponent.parentNode); }
		catch(e){ console.log("No node selected."); }
	}


	function loadUpdateAbleComponent(){
		// allow CRUD on edit mode only
		if(currentMode != "Edit"){ return; }

		try{
			var component = currentComponent.parentNode.firstChild;
			$("#updateCompPanel").text($(component).find("#bhsComponent-editable").html());
		}
		catch(e){ console.log("No node selected."); }
	}


	function updateComponent(){
		// allow CRUD on edit mode only
		if(currentMode != "Edit"){ return; }

		try{
			var component = currentComponent.parentNode.firstChild;
			$(component).find("#bhsComponent-editable").html($("#updateCompPanel").text());
		}
		catch(e){ console.log("No node selected."); }
	}



// LOCAL STORAGE ----------------------------------
	// confirm if localstorage item exist
	if(localStorage.getItem("stagedBuilder") === null){ location.replace("/"); }
	else{ 
		var temp = JSON.parse(localStorage.getItem("stagedBuilder"));
		formToken = temp["token"];
		responseToken = temp["response_id"];
		$("#formTitle").val(temp["title"]);
		// console.log(temp);
		
		if(formToken != null && formToken != ""){ 
			setMode("Edit");
			$("#labelSaver").text("Form");
			$(document.getElementById("Response").parentNode).hide();
			$(document.getElementById("Publish").parentNode).hide();
			try{
				loadComponents(JSON.parse(JSON.parse(localStorage.getItem("stagedBuilder"))["formatted"]));
			}
			catch(err){ 
				// most likely empty form template.
				console.log("form template has no components");
			}
		}

		if(responseToken != null && responseToken != ""){
			$("#labelSaver").text("Response to Form");
			$("#formTitle").prop('disabled', true);
			$(document.getElementById("Edit").parentNode).hide();
			$(document.getElementById("Preview").parentNode).hide();

			$(document.getElementById("componentList")).hide();
			$(document.getElementById("pinunpintList")).hide();
			$(document.getElementById("updaterList")).hide();
			$(document.getElementById("deletertList")).hide();
			$(document.getElementById("templateList")).hide();
			try{
				loadComponents(JSON.parse(JSON.parse(localStorage.getItem("stagedBuilder"))["form_content"]["formatted"]));
			}
			catch(err){ 
				// most likely empty form template.
				console.log("form template has no components");
			}
			
			setMode("Response");
		}

		$(window).bind("beforeunload", function(event) {
			return "Make sure you have saved your changes";
		});
	}