-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2017 at 03:42 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bhs_user`
--

-- --------------------------------------------------------

--
-- Table structure for table `bhs_form_history`
--

CREATE TABLE IF NOT EXISTS `bhs_form_history` (
  `_id` int(11) NOT NULL,
  `_history_author` varchar(200) NOT NULL,
  `_history_doc` varchar(200) NOT NULL,
  `_history_event` text NOT NULL,
  `_history_stash` longtext NOT NULL,
  `_history_commit` longtext NOT NULL,
  `_history_share` text NOT NULL,
  `_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bhs_form_history`
--

INSERT INTO `bhs_form_history` (`_id`, `_history_author`, `_history_doc`, `_history_event`, `_history_stash`, `_history_commit`, `_history_share`, `_date_time`) VALUES
(6, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'x7smmvo6zhyz83j9zuflgnwmi8nptn14c3b2n5y297pb9ctbj4i', 'onCreateForm', '', '', '["cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', '2017-02-25 07:21:44'),
(7, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'cm7e0nqr2xwa8huhfu5q7u8frf7t18yfl2r551vl69qmrq9f6r', 'onCreateForm', '', '', '["cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', '2017-02-25 08:23:54'),
(8, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'fiarfa8or8rlnbshb4iu9885mi2434gjdie3ckw711b64y7rpb9', 'onCreateForm', '', '', '["cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', '2017-02-25 09:16:53'),
(9, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', '54zdwuwcy78nr1h0sgbzp30udi782mtvdmczxagrsr9j9xry66r', 'onCreateForm', '', '', '["0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr"]', '2017-02-25 09:18:10'),
(10, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'cm7e0nqr2xwa8huhfu5q7u8frf7t18yfl2r551vl69qmrq9f6r', 'onResponse', '', '', '', '2017-02-25 09:30:56'),
(11, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'fiarfa8or8rlnbshb4iu9885mi2434gjdie3ckw711b64y7rpb9', 'onResponse', '', '', '', '2017-02-25 10:04:33'),
(12, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'cm7e0nqr2xwa8huhfu5q7u8frf7t18yfl2r551vl69qmrq9f6r', 'onResponse', '', '', '', '2017-02-25 11:55:15'),
(13, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'cm7e0nqr2xwa8huhfu5q7u8frf7t18yfl2r551vl69qmrq9f6r', 'onResponse', '', '', '', '2017-02-25 12:06:42'),
(14, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'x7smmvo6zhyz83j9zuflgnwmi8nptn14c3b2n5y297pb9ctbj4i', 'onResponse', '', '', '', '2017-02-25 12:08:00'),
(15, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', '89v0igzllqpplled4d39mgqfr9r650uok8k8hkulq1x5dvlsor', 'onCreateForm', '', '', '["cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', '2017-02-25 12:19:28'),
(16, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', '89v0igzllqpplled4d39mgqfr9r650uok8k8hkulq1x5dvlsor', 'onShareForm', '', '', '["0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr","cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', '2017-02-25 13:21:47'),
(17, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', '89v0igzllqpplled4d39mgqfr9r650uok8k8hkulq1x5dvlsor', 'onShareForm', '', '', '["0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr","cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', '2017-02-25 13:22:42'),
(18, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', '89v0igzllqpplled4d39mgqfr9r650uok8k8hkulq1x5dvlsor', 'onShareForm', '', '', '["cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', '2017-02-25 13:25:03'),
(19, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', '89v0igzllqpplled4d39mgqfr9r650uok8k8hkulq1x5dvlsor', 'onShareForm', '', '', '["0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr","cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', '2017-02-25 13:25:17'),
(20, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'x7smmvo6zhyz83j9zuflgnwmi8nptn14c3b2n5y297pb9ctbj4i', 'onShareForm', '', '', '["0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr","cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', '2017-02-25 13:25:32'),
(21, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', '9g5z2xxvrapb1z0v61k4eu3diltymk5i280se8hv61jbkv5cdi', 'onCreateForm', '', '', '["0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr"]', '2017-02-26 04:13:14'),
(22, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', '9g5z2xxvrapb1z0v61k4eu3diltymk5i280se8hv61jbkv5cdi', 'onShareForm', '', '', '["cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or","0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr"]', '2017-02-26 04:14:06'),
(23, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', '9g5z2xxvrapb1z0v61k4eu3diltymk5i280se8hv61jbkv5cdi', 'onResponse', '', '', '', '2017-02-26 04:14:51'),
(24, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', '9g5z2xxvrapb1z0v61k4eu3diltymk5i280se8hv61jbkv5cdi', 'onResponse', '', '', '', '2017-02-26 04:15:40'),
(25, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', '89v0igzllqpplled4d39mgqfr9r650uok8k8hkulq1x5dvlsor', 'onResponse', '', '', '', '2017-02-26 04:16:59'),
(26, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'gkf52xgcs0idq4mn6zlbrcnmibo48sb3sxj0ukgdffdu7ynwmi', 'onCreateForm', '', '', '["cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', '2017-02-26 06:52:03'),
(27, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'gkf52xgcs0idq4mn6zlbrcnmibo48sb3sxj0ukgdffdu7ynwmi', 'onDeleteForm', '', '', '', '2017-02-26 07:05:53'),
(28, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'z0r5isj16f7o8rt49yp1wz5mi73muonnyxa1i8z4nqevpxh6w29', 'onCreateForm', '', '', '["cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', '2017-02-26 07:06:17'),
(29, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'z0r5isj16f7o8rt49yp1wz5mi73muonnyxa1i8z4nqevpxh6w29', 'onShareForm', '', '', '["0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr","cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', '2017-02-26 07:06:30'),
(30, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'z0r5isj16f7o8rt49yp1wz5mi73muonnyxa1i8z4nqevpxh6w29', 'onResponse', '', '', '', '2017-02-26 07:06:42'),
(31, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'z0r5isj16f7o8rt49yp1wz5mi73muonnyxa1i8z4nqevpxh6w29', 'onResponse', '', '', '', '2017-02-26 07:07:45'),
(32, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'z0r5isj16f7o8rt49yp1wz5mi73muonnyxa1i8z4nqevpxh6w29', 'onResponse', '', '', '', '2017-02-26 07:10:31'),
(33, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'z0r5isj16f7o8rt49yp1wz5mi73muonnyxa1i8z4nqevpxh6w29', 'onDeleteForm', '', '', '', '2017-02-26 07:11:16'),
(34, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', '5ka5rq8sj3c2g0zr8tehk0rudiqoixrz5x46u360rzrpvw8kt9', 'onDeleteResponse', '', '', '', '2017-02-26 07:32:40'),
(35, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'z0r5isj16f7o8rt49yp1wz5mi73muonnyxa1i8z4nqevpxh6w29', 'onDeleteResponse', '', '', '', '2017-02-26 07:35:22'),
(36, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', '89v0igzllqpplled4d39mgqfr9r650uok8k8hkulq1x5dvlsor', 'onDeleteResponse', '', '', '', '2017-02-26 07:35:37');

-- --------------------------------------------------------

--
-- Table structure for table `bhs_form_response`
--

CREATE TABLE IF NOT EXISTS `bhs_form_response` (
  `_id` int(11) NOT NULL,
  `_response_token` varchar(200) NOT NULL,
  `_form_token` varchar(200) NOT NULL,
  `_raw_html_withAns` longtext NOT NULL,
  `_formatted_withAns` longtext NOT NULL,
  `_respondent_token` varchar(200) NOT NULL,
  `_delete` tinyint(1) NOT NULL DEFAULT '0',
  `_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bhs_form_response`
--

INSERT INTO `bhs_form_response` (`_id`, `_response_token`, `_form_token`, `_raw_html_withAns`, `_formatted_withAns`, `_respondent_token`, `_delete`, `_date_time`) VALUES
(5, '4gnv0yc3im0xaw5rmilb9jxlxr9rhfrnv03n8b3y4mxvtm8zd7vi', '89v0igzllqpplled4d39mgqfr9r650uok8k8hkulq1x5dvlsor', '', '', 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 1, '2017-02-26 04:16:59'),
(8, '5ka5rq8sj3c2g0zr8tehk0rudiqoixrz5x46u360rzrpvw8kt9', 'z0r5isj16f7o8rt49yp1wz5mi73muonnyxa1i8z4nqevpxh6w29', '', '', 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 1, '2017-02-26 07:10:31'),
(7, '60tywqyylwloe7hx172a5ah5mic7g4xgh4ic1d1qzibhcs7nwmi', 'z0r5isj16f7o8rt49yp1wz5mi73muonnyxa1i8z4nqevpxh6w29', '', '', 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 1, '2017-02-26 07:07:44'),
(4, 'caqw86itfhgiqsek3i35wmi4ioyumwekounhf7287eu7n9udi', '9g5z2xxvrapb1z0v61k4eu3diltymk5i280se8hv61jbkv5cdi', '', '', '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 0, '2017-02-26 04:15:40'),
(6, 'h3bv3a3lud39uw4fz08i4fgvily4xhd7tfh5wgu7zcu1c680k9', 'z0r5isj16f7o8rt49yp1wz5mi73muonnyxa1i8z4nqevpxh6w29', '', '', 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 0, '2017-02-26 07:06:42'),
(1, 'ro5rr4ccqdtxu8yvxdf9r7ldiqh3j1hzig4uv6q861m7p3c8fr', 'cm7e0nqr2xwa8huhfu5q7u8frf7t18yfl2r551vl69qmrq9f6r', '', '', 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 0, '2017-02-25 12:06:42'),
(2, 'ti1opl00ge6gvt9adpz2utyb9eyks5vwo4nommibjx35scq5mi', 'x7smmvo6zhyz83j9zuflgnwmi8nptn14c3b2n5y297pb9ctbj4i', '', '', 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 0, '2017-02-25 12:08:00'),
(3, 'y1zmjbxv2wbn6esk21xuhaorj4lwsumyyux71jjytwqh3erk9', '9g5z2xxvrapb1z0v61k4eu3diltymk5i280se8hv61jbkv5cdi', '', '', 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 0, '2017-02-26 04:14:51');

-- --------------------------------------------------------

--
-- Table structure for table `bhs_form_templates`
--

CREATE TABLE IF NOT EXISTS `bhs_form_templates` (
  `_id` int(11) NOT NULL,
  `_doc_token` varchar(200) NOT NULL,
  `_author` varchar(200) NOT NULL,
  `form_title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `_raw_html` longtext NOT NULL,
  `_formatted` longtext NOT NULL,
  `_shared_arr` text NOT NULL,
  `_delete` tinyint(1) NOT NULL DEFAULT '0',
  `_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bhs_form_templates`
--

INSERT INTO `bhs_form_templates` (`_id`, `_doc_token`, `_author`, `form_title`, `description`, `_raw_html`, `_formatted`, `_shared_arr`, `_delete`, `_date_time`) VALUES
(15, '54zdwuwcy78nr1h0sgbzp30udi782mtvdmczxagrsr9j9xry66r', '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 'new testing form', '', '', '', '["0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr"]', 0, '2017-02-25 09:18:10'),
(16, '89v0igzllqpplled4d39mgqfr9r650uok8k8hkulq1x5dvlsor', 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'temp 4th', '', '', '', '["0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr","cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', 0, '2017-02-25 12:19:28'),
(17, '9g5z2xxvrapb1z0v61k4eu3diltymk5i280se8hv61jbkv5cdi', '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 'this is for SHARING', '', '', '', '["cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or","0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr"]', 0, '2017-02-26 04:13:14'),
(13, 'cm7e0nqr2xwa8huhfu5q7u8frf7t18yfl2r551vl69qmrq9f6r', 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'my 2nd form', '', '', '', '["cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', 0, '2017-02-25 08:23:54'),
(14, 'fiarfa8or8rlnbshb4iu9885mi2434gjdie3ckw711b64y7rpb9', 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'testing', '', '', '', '["cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', 0, '2017-02-25 09:16:52'),
(18, 'gkf52xgcs0idq4mn6zlbrcnmibo48sb3sxj0ukgdffdu7ynwmi', 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', '1st form for delete', '', '', '', '["cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', 1, '2017-02-26 06:52:03'),
(12, 'x7smmvo6zhyz83j9zuflgnwmi8nptn14c3b2n5y297pb9ctbj4i', 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'My 1st test Form', '', '', '', '["0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr","cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', 0, '2017-02-25 07:21:43'),
(19, 'z0r5isj16f7o8rt49yp1wz5mi73muonnyxa1i8z4nqevpxh6w29', 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'form with response', '', '', '', '["0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr","cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or"]', 1, '2017-02-26 07:06:17');

-- --------------------------------------------------------

--
-- Table structure for table `bhs_user_attendance`
--

CREATE TABLE IF NOT EXISTS `bhs_user_attendance` (
  `_id` int(11) NOT NULL,
  `_user_token` varchar(200) NOT NULL,
  `_event` varchar(25) NOT NULL,
  `_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bhs_user_attendance`
--

INSERT INTO `bhs_user_attendance` (`_id`, `_user_token`, `_event`, `_date_time`) VALUES
(1, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-25 03:09:28'),
(2, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-25 03:11:57'),
(3, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-25 03:12:29'),
(4, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-25 03:13:28'),
(5, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-25 03:21:30'),
(6, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-25 03:22:20'),
(7, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-25 03:28:25'),
(8, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-25 03:55:03'),
(9, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-25 04:06:03'),
(10, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-25 04:21:28'),
(11, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 'onLogin', '2017-02-25 04:21:35'),
(12, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 'onLogout', '2017-02-25 06:42:36'),
(13, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-25 06:42:45'),
(14, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-25 08:00:59'),
(15, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-25 08:01:08'),
(16, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-25 08:02:40'),
(17, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 'onLogin', '2017-02-25 08:02:49'),
(18, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 'onLogout', '2017-02-25 08:04:46'),
(19, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-25 08:04:54'),
(20, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-25 09:17:06'),
(21, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-25 09:17:48'),
(22, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-25 09:17:52'),
(23, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 'onLogin', '2017-02-25 09:18:01'),
(24, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 'onLogout', '2017-02-25 09:19:07'),
(25, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-25 09:19:27'),
(26, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 'onLogin', '2017-02-26 04:10:16'),
(27, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 'onLogout', '2017-02-26 04:14:09'),
(28, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-26 04:14:22'),
(29, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-26 04:15:01'),
(30, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 'onLogin', '2017-02-26 04:15:12'),
(31, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 'onLogout', '2017-02-26 04:15:52'),
(32, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-26 04:16:08'),
(33, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-26 04:21:24'),
(34, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-26 04:27:38'),
(35, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-26 04:54:06'),
(36, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-26 04:54:06'),
(37, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-26 06:51:54'),
(38, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-26 07:35:41'),
(39, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 'onLogin', '2017-02-26 07:35:49'),
(40, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-26 08:41:27'),
(41, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-26 08:52:58'),
(42, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogin', '2017-02-26 08:53:10'),
(43, 'cfeyr26rjszzlt6dd5o4dkj4i4f3h1cg9sq3gdf4mkkkyj2x1or', 'onLogout', '2017-02-26 14:04:24'),
(44, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 'onLogin', '2017-02-26 14:04:32'),
(45, '0hxmf39ootu1xry5z3ael2qpvi6x34x5np1o8xycawn0bxzuxr', 'onLogout', '2017-02-26 14:41:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bhs_form_history`
--
ALTER TABLE `bhs_form_history`
  ADD KEY `_id` (`_id`),
  ADD KEY `_history_author` (`_history_author`),
  ADD KEY `_doc_token` (`_history_doc`);

--
-- Indexes for table `bhs_form_response`
--
ALTER TABLE `bhs_form_response`
  ADD UNIQUE KEY `_response_token` (`_response_token`),
  ADD KEY `_id` (`_id`),
  ADD KEY `_form_token` (`_form_token`),
  ADD KEY `_respondent_token` (`_respondent_token`);

--
-- Indexes for table `bhs_form_templates`
--
ALTER TABLE `bhs_form_templates`
  ADD PRIMARY KEY (`_doc_token`),
  ADD UNIQUE KEY `form_title` (`form_title`),
  ADD KEY `_id` (`_id`),
  ADD KEY `_author` (`_author`);

--
-- Indexes for table `bhs_user_attendance`
--
ALTER TABLE `bhs_user_attendance`
  ADD KEY `_user_token` (`_user_token`),
  ADD KEY `_id` (`_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bhs_form_history`
--
ALTER TABLE `bhs_form_history`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `bhs_form_response`
--
ALTER TABLE `bhs_form_response`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `bhs_form_templates`
--
ALTER TABLE `bhs_form_templates`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `bhs_user_attendance`
--
ALTER TABLE `bhs_user_attendance`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bhs_form_response`
--
ALTER TABLE `bhs_form_response`
  ADD CONSTRAINT `bhs_form_response_ibfk_1` FOREIGN KEY (`_form_token`) REFERENCES `bhs_form_templates` (`_doc_token`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
