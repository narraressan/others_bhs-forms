// usage: nodemon bhs-user-server.js
// usage: node bhs-user-server.js

// linux
// usage: node bhs-user-server.js



// import utilities
const reqExpress = require("express"); 
const reqIPFilter = require("express-ipfilter").IpFilter;
const reqbodyParser = require("body-parser");
const reqfileUpload = require("express-fileupload");
const reqPath = require("path");
const reqMySQL = require("mysql");
const reqFs = require("fs");
const reqUtil = require("util");
const reqCrypto = require("crypto");



// custom console.log function to write text file for monitoring
	var logName = "user-backend-logs.txt";
	var logFile = reqFs.createWriteStream(logName, { flags: "a" });
	var logStdout = process.stdout;
	console.log = function () {
		logFile.write(reqUtil.format.apply(null, arguments) + "\n");
		logStdout.write(reqUtil.format.apply(null, arguments) + "\n");
	}
	console.error = console.log;
// --------------------------------------------------------



// default values
	var mysql_bhsAdmin = reqMySQL.createConnection({ host: "localhost", port: "3306", user: "nto2017", password: "LionCity", database: "bhs_admin"});
	var mysql_bhsUser = reqMySQL.createConnection({ host: "localhost", port: "3306", user: "nto2017", password: "LionCity", database: "bhs_user"});
	var express = reqExpress();
	var config = {
		"default-port": 80,
		"blacklisted": [],
		"en-algorithm": "aes-256-ctr",
		"en-key": "2012-0129-2012-0422"
	};
	// express.use(reqIPFilter(config["blacklisted"])); ---> use this when you intend to block an IP
	express.use(reqbodyParser.urlencoded({ extended: false, limit: '50mb' }));
	express.use(reqbodyParser.json({ limit: '50mb' }));
	express.use(reqfileUpload());

	// default bhs-lab fron-end page
	express.get("/", (req, res) => { res.sendFile(reqPath.join(__dirname + "/../frontend/index.html")); });
	express.get("/bhs-lab", (req, res) => { res.sendFile(reqPath.join(__dirname + "/../frontend/bhs-lab-dashboard.html")); });
	express.use("/frontend", reqExpress.static(reqPath.join(__dirname, "/../frontend")));

	// builder - v1.0 - directory (css & js files)
	express.use("/bhs-builder", reqExpress.static(reqPath.join(__dirname, "/../builder")));
	express.use("/bhs-pdf", reqExpress.static(reqPath.join(__dirname, "/../builder")));
	express.use("/bhs-gallery", reqExpress.static(reqPath.join(__dirname, "/../builder")));
	express.use("/bhs-components", reqExpress.static(reqPath.join(__dirname, "/../builder")));
	express.use("/bhs-builder-2.0", reqExpress.static(reqPath.join(__dirname, "/../builder-2.0")));
	express.use("/form-fill-up", reqExpress.static(reqPath.join(__dirname, "/../forms")));

	// builder - v1.0 - source file
	express.get("/bhs-builder", (req, res) => { res.sendFile(reqPath.join(__dirname + "/../builder/builder.html")); });
	express.get("/bhs-pdf", (req, res) => { res.sendFile(reqPath.join(__dirname + "/../builder/pdf.html")); });
	express.get("/bhs-gallery", (req, res) => { res.sendFile(reqPath.join(__dirname + "/../builder/gallery.html")); });
	express.get("/bhs-components", (req, res) => { res.sendFile(reqPath.join(__dirname + "/../builder/components.html")); });
	express.get("/bhs-builder-2.0", (req, res) => { res.sendFile(reqPath.join(__dirname + "/../builder-2.0/builder.html")); });
	express.get("/form-fill-up", (req, res) => { res.sendFile(reqPath.join(__dirname + "/../forms/form-loader.html")); });

	// config file
	express.use("/config", reqExpress.static(reqPath.join(__dirname, "/../backend")));
// --------------------------------------------------------



// default functions
	
	// NOTES:
	// encryption and decryption source: http://lollyrock.com/articles/nodejs-encryption/

	function encrypt(text){
		var cipher = reqCrypto.createCipher(config["en-algorithm"], config["en-key"]);
		var crypted = cipher.update(text, "utf8", "hex");
		crypted += cipher.final("hex");
		return crypted;
	}

	function decrypt(text){
		var decipher = reqCrypto.createDecipher(config["en-algorithm"], config["en-key"]);
		var dec = decipher.update(text, "hex", "utf8");
		dec += decipher.final("utf8");
		return dec;
	}

	function rand(){ return Math.random().toString(36).substr(2); };

	function validateAuth(auth, reqData, responseData, res, successCallback, errorCallback){
		// console.log(auth + " " + reqData + " " + responseData + " " + res + " " + successCallback + " " + errorCallback);
		// get data from mysql
		mysql_bhsAdmin.query("SELECT `_token` FROM `bhs_users` WHERE BINARY `_token`=?", [auth], function (error, results, fields) {
			if(error){ 
				console.log(error);
				errorCallback();
			}
			else{
				if(results.length == 1){ successCallback(); }
				else{ errorCallback(); }
			}
		});
	}
// --------------------------------------------------------



// RESTful endpoints
	express.get("/bhs-lab-server/check-activity", (req, res) => {
		res.send("BHS - Lab server active - " + new Date());
	});

	// upload to gallery
		express.post("/bhs-lab-server/upload", function(req, res) {
			if (!req.files) { return res.status(400).send('No files were uploaded.'); }

			var sampleFile = req.files.sampleFile;
			var filename = rand() + "-" + rand() + "-" + sampleFile.name;

			sampleFile.mv("../builder/gallery/" + filename, function(err) {
				if (err) { return res.status(500).send(err); }
				res.send('File uploaded!');
			});
		});

	// get gallery content
		express.get("/bhs-lab-server/gallery", function(req, res) {
			reqFs.readdir("../builder/gallery/", (err, files) => {
				res.send(files);
			})
		});

	// login user account
		express.post("/bhs-lab-server/user-login", (req, res) => {
			var responseData = { "message": null, "data": null };

			var reqData = req.body;

			// get data from mysql
			mysql_bhsAdmin.query("SELECT `bhs_email`, `bhs_password`, `_token` FROM `bhs_users` WHERE BINARY `bhs_email`=? and `bhs_password`=?", [reqData["email"], encrypt(reqData["password"])], function (error, results, fields) {
				if(error){ 
					console.log(error);
					responseData["message"] = "Sorry, I don't know you."; 
					res.send(responseData);
				}
				else{
					if(results.length == 1){ 
						// update attendance
						mysql_bhsUser.query("INSERT INTO `bhs_user_attendance`(`_user_token`, `_event`) VALUES (?, ?)", [results[0]["_token"], "onLogin"], function (error, docs, fields) {
							if(error){ 
								console.log(error);
								responseData["message"] = "Oops! Error attendance check, please try again later.";
							}
							else{ 
								responseData["message"] = "User validated. Welcome!"; 
								responseData["data"] = results;
							}

							res.send(responseData);
						});
						// ---------------------------------------
					}
					else if(results.length > 1){ 
						console.log("multiple results: " + reqData["bhs_email"] + " | " + encrypt(reqData["password"]));
						responseData["message"] = "I am detecting an illegal access. Reporting issue to admin..."; 
						res.send(responseData);
					}
					else{
						// no result
						responseData["message"] = "Sorry, but I don't know you."; 
						res.send(responseData);
					}
				}
			});
		});

	// Logout user account
		express.post("/bhs-lab-server/user-logout", (req, res) => {
			var responseData = { "message": null, "data": null };

			var auth = req.header("Authorization");

			var successCallback = function(){
				// update attendance 
				mysql_bhsUser.query("INSERT INTO `bhs_user_attendance`(`_user_token`, `_event`) VALUES (?, ?)", [auth, "onLogout"], function (error, results, fields) {
					if(error){ 
						console.log(error);
						responseData["message"] = "Oops! Error attendance check, please try again later.";
					}
					else{ 
						responseData["message"] = "User logout successfully. Bye!"; 
						responseData["data"] = 200;
					}

					res.send(responseData);
				});
				// ---------------------------------------
			}

			var errorCallback = function(){
				responseData["message"] = "Oops! Invalid authentication key."; 
				res.send(responseData);
			}

			validateAuth(auth, null, responseData, res, successCallback, errorCallback);
		});

	// update profile password
		express.post("/bhs-lab-server/update-user-data", (req, res) => {
			var responseData = { "message": null, "data": null };

			var auth = req.header("Authorization");
			var reqData = req.body;

			var successCallback = function(){
				mysql_bhsAdmin.query("UPDATE `bhs_users` set `bhs_password`=? WHERE `_token`=?", [encrypt(reqData["new_pass"]), auth], function (error, results, fields) {
					if(error){ 
						console.log(error);
						responseData["message"] = "Oops! Unable to process update, please try again later.";
					}
					else{ 
						responseData["message"] = "User successfully updated."; 
						responseData["data"] = 200;
					}

					res.send(responseData);
				});
			}

			var errorCallback = function(){
				responseData["message"] = "Oops! Invalid authentication key."; 
				res.send(responseData);
			}

			validateAuth(auth, reqData, responseData, res, successCallback, errorCallback);
		});

	//-------------------------------------------------------
	// STATUS -----------------------------------------------
		express.get("/bhs-lab-server/user-getAllStatus", (req, res) => {
			var responseData = { "message": null, "data": null };

			var auth = req.header("Authorization");

			// NO ACCESS VALIDATION REQUIRED!
			// retrieve user attendance
			mysql_bhsUser.query("SELECT b.bhs_email as 'email', a._user_token as 'token', max(a._event) as 'status', DATE_FORMAT(max(a._date_time), '%m-%d-%Y %h:%m %p') as 'time' FROM bhs_user_attendance a, bhs_admin.bhs_users b WHERE BINARY a._user_token = b._token group by a._user_token", [], function (error, results, fields) {
				if(error){ 
					console.log(error);
					responseData["message"] = "Oops! Error building user circle.";
				}
				else{ 
					responseData["message"] = "You circle has been found."; 
					responseData["data"] = results;
				}

				res.send(responseData);
			});
			// ---------------------------------------
		});

	// HISTORY ----------------------------------------------
		express.get("/bhs-lab-server/user-getAllActHistory", (req, res) => {
			var responseData = { "message": null, "data": null };

			var auth = req.header("Authorization");

			// NO ACCESS VALIDATION REQUIRED!
			// retrieve user attendance
			mysql_bhsUser.query("SELECT c.`bhs_email` as 'email', a.`_history_author` as 'token', b.`form_title` as 'title', a.`_history_event` as 'status', DATE_FORMAT(a.`_date_time`, '%m-%d-%Y %h:%m %p') as 'time' FROM bhs_form_history a, bhs_form_templates b, bhs_admin.bhs_users c WHERE BINARY a.`_history_doc` = b.`_doc_token` and b.`_author` = c.`_token` and (b.`_shared_arr` like ? or b.`_author` = ?) group by a.`_id` order by a.`_date_time` desc", ["%" + auth + "%", auth], function (error, results, fields) {
				if(error){ 
					console.log(error);
					responseData["message"] = "Oops! Error building user circle.";
				}
				else{ 
					responseData["message"] = "Here's the activities in your circle."; 
					responseData["data"] = results;
				}

				res.send(responseData);
			});
			// ---------------------------------------
		});

	// STATIC - FORMS ---------------------------------------
		// get static forms
		// cases:
			// get one form (in: form-key | out: form)
			// get all forms (in: null | out: forms)
			express.get("/bhs-lab-server/user-getAllStaticForms", (req, res) => {
				var responseData = { "message": null, "data": null };

				var key = req.query["key"];

				// hard coded forms - this state is temporary, should make a database for this one.
				var staticForms = [
					{ title: "ATP Personal Contents Testing", token: "k16khnyt2rq4fmzi5opwuerk97ig1f13i344bw2x0f2n8eel8fr", author: "@bhs", time: "" },
					{ title: "Chain of Custody Form - Spore Traps & Tape Lifts", token: "7ne42atkla4tfx7kj82jmaq0k9admx4219r9vbjeezvbxz5b3xr", author: "@bhs", time: "" },
					{ title: "Colony Count Per Cubic Metre", token: "j7nx1hbmsn85ebad4w81l9pb9rv33jhlm6wl5bmc1owc7syvi", author: "@bhs", time: "" },
					{ title: "DIY Mould Test Kits - Sample 090217", token: "7l3neyjn4mb8g7on3ee2whr529bjnsb7vbdlmq2wi0z5tsp2e29", author: "@bhs", time: "" },
					{ title: "IAQ - Lab ID Unknown Mould Test - Microbiology Results & Recommendation", token: "zp452cboymadd7iyjgw0icnmi0p3elt0mih9dc03xqls80py14i", author: "@bhs", time: "" },
					{ title: "IAQ Reports - Sample 090217", token: "ilbvoc8xncacnccthwl0ltyb9m3mprp0yq3t8vzq1a4umwvcxr", author: "@bhs", time: "" },
					{ title: "IAQ Version 1", token: "2hy7oele9injcg6fc8yfrbe297sol10og5s915qo64eokr19k9", author: "@bhs", time: "" },
					{ title: "Indoor Air Quality & Mould Inspection Report (24 Samples)", token: "i3b6rgrdtcnbpk66jhalwhfrto6zaqof04dwizpuqgvr96bt9", author: "@bhs", time: "" },
					{ title: "Infection Control Environmental Health Audit", token: "3mr1dpsmqsxiqv91b3ifz85miw33vz7dgdfousvcr7c14cayvi", author: "@bhs", time: "" },
					{ title: "Lab ID Unknown Mould Test - Microbiology Results", token: "lqwizb41p15jlnsbt7vydgqfromkwqmqc3vqku8wod1ispp66r", author: "@bhs", time: "" },
					{ title: "Lab ID Unknown Mould Test - Microbiology Results (Ver1216)", token: "oril4moeeojuu1m66fiqf47viov4cdunkvexvhsf3bhdunmi", author: "@bhs", time: "" },
					{ title: "Lab ID Unknown Mould Test - Microbiology Results (Ver2)", token: "jv2uc6ckufnfr4fo16v2njyvi6d7t4u7dyd1ogm3kaf82vjkyb9", author: "@bhs", time: "" },
					{ title: "Mould Test Kit Microbiology Results", token: "l3powa1uc6puvomuciukmaemir5zrg72j3t7m0nbo8rvrc0udi", author: "@bhs", time: "" },
					{ title: "Mould Test Kit Microbiology Results (Copy)", token: "tkk328u5a8ov471ov4kfkcsor3qwrf8pekpoqvukidwbij54s4i", author: "@bhs", time: "" },
					{ title: "Mould Test Kit Microbiology Results 4-Sample Kit", token: "ib4a0rmci4nto7roftbne9udizfynsprja8ofzt53830vsra4i", author: "@bhs", time: "" },
					{ title: "Spore Trap Analysis", token: "s4ku2xpu4ctmue8st9w7phkt9kr49n3uvs1df6wurwp26xyldi", author: "@bhs", time: "" },
					{ title: "Spore Trap Analysis (Copy Backup)", token: "2jb17ztmxp0pql63d5copnl8frha9ssm8wc37vk3ajdrr5ghkt9", author: "@bhs", time: "" },
					{ title: "Spore Trap Analysis - Fungal ID & Count (20 Field)", token: "agvjwxzsiypwofwe76o0i19k9z88t2cz74cw6jkp0rthgp66r", author: "@bhs", time: "" },
					{ title: "Spore Traps - Sample 090217", token: "mhvatnv7b9el7apw9vj5ah5mifh3j4d32fjbpnsz50l3yuv7vi", author: "@bhs", time: "" },
					{ title: "Tape Lift - Sample 090217", token: "c5e023lusdgsif9l9jvtv5cdi96rizcixeyp4yaz7q72nbfbt9", author: "@bhs", time: "" },
					{ title: "Tape Lift - Total Count & Identification", token: "9pqxgs4gzdecd65vitl7919k9gmrffy3i3e415u1ip6r7tx1or", author: "@bhs", time: "" },
					{ title: "Tape Lift - Total Count & Identification (12 Sample Bio-Tape)", token: "2qko9m3hmybnx0fug1c3wpzaorj9qp3mx61isrgfyxxb580vn29", author: "@bhs", time: "" },
					{ title: "Tape Lift - Total Count & Identification (12 Sample Input)", token: "7ihpu3nyfhex48j3ts4755qaor7b9pb5h0rizwl5fdjk8a6y9zfr", author: "@bhs", time: "" },
					{ title: "Tape Lift - Total Count & Identification (15 Sample Bio-Tape)", token: "c0mfzy4k3pfdmufy9hf0nqaorl9rh8i0s6itbmivywbwewmi", author: "@bhs", time: "" },
					{ title: "Tape Lift - Total Count & Identification (Copy to make 12 fields)", token: "op8akv6xgxe80zx0undlmzpvirlnmasyt7apg2f72bykbro1or", author: "@bhs", time: "" },
					{ title: "Tape Lift - Total Count & Identification (Home Contents 12 Sample)", token: "d8h6q4i1bos21zwama8e8w7b99h95cn0mj5hjckorbxona5rk9", author: "@bhs", time: "" }
				]

				if(key == "" || key == null){
					responseData["message"] = "Static forms retrieved.";
					responseData["data"] = staticForms;
				}
				else{					
					var theForm = null;
					for (var i = 0; i < staticForms.length; i++) {
						if(staticForms[i]["token"] == key){ 
							responseData["data"] = [staticForms[i]]; 
							break;
						}
					}

					responseData["message"] = responseData["data"]["form"] + " form identified.";
				}

				res.send(responseData);
			});

	// FORMS ------------------------------------------------
		// get form/s
		// cases:
			// get one form for download, update or etc (in: auth, form id | out: form)
			// get all forms related (shared and authored) to user (in: auth (as author) | out: forms)
			express.get("/bhs-lab-server/user-getAllForms", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.query;

				var successCallback = function(){
					var queryString = "SELECT `_doc_token` as 'token', `_author` as 'author', `form_title` as 'title', `_raw_html` as 'raw', `_formatted` as 'formatted', `_shared_arr` as 'shared', DATE_FORMAT(`_date_time`, '%m-%d-%Y %h:%m %p') as 'time' FROM `bhs_form_templates` WHERE BINARY `_delete` = 0 and (`_author` = ? or `_shared_arr` like ?)";
					var queryValues = [auth, "%" + auth + "%"];

					if(reqData["form_id"] != ""){ 
						queryString += " and `_doc_token` = ?"; 
						queryValues.push(reqData["form_id"]);
					}
					queryString += " order by `_date_time` desc";

					mysql_bhsUser.query(queryString, queryValues, function (error, results, fields) {
						if(error){ 
							console.log(error);
							responseData["message"] = "Oops! Unable to get forms, please try again later.";
						}
						else{ 
							responseData["message"] = "Form/s list found."; 
							responseData["data"] = results;
						}

						res.send(responseData);
					});
				}

				var errorCallback = function(){
					responseData["message"] = "Oops! Invalid authentication key."; 
					res.send(responseData);
				}

				validateAuth(auth, reqData, responseData, res, successCallback, errorCallback);
			});

		// create forms
		// cases: 
			// create new form and trigger activity history (in: auth (as author), form name | out: 200)
			express.post("/bhs-lab-server/user-createNewForm", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				var successCallback = function(){
					var temp_doc_token = rand() + rand();
					var temp_shared_arr = JSON.stringify([auth]);

					mysql_bhsUser.query("INSERT INTO `bhs_form_templates`(`_doc_token`, `_author`, `form_title`, `_shared_arr`) VALUES (?, ?, ?, ?)", [temp_doc_token, auth, reqData["form_name"], temp_shared_arr], function (error, results, fields) {
						if(error){ 
							console.log(error);
							responseData["message"] = "Oops! Unable to create form, please try again later or change the form name.";
							res.send(responseData);
						}
						else{ 
							mysql_bhsUser.query("INSERT INTO `bhs_form_history`(`_history_author`, `_history_event`, `_history_share`, `_history_doc`) VALUES (?, ?, ?, ?)", [auth, "onCreateForm", temp_shared_arr, temp_doc_token], function (error, results, fields) {
								if(error){
									console.log(error);
									responseData["message"] = "Oops! Unable to update history. Please try again later.";
								}
								else{
									responseData["message"] = "Form created, wait or open the builder."; 
									responseData["data"] = temp_doc_token;
								}

								res.send(responseData);
							});
						}
					});
				}

				var errorCallback = function(){
					responseData["message"] = "Oops! Invalid authentication key."; 
					res.send(responseData);
				}

				validateAuth(auth, reqData, responseData, res, successCallback, errorCallback);
			});

		// update form content
		// cases:
			// update form and trigger activity history (in: auth (as author), form content (formatted, raw), form id, form title | out: 200)
			express.post("/bhs-lab-server/user-updateForm", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				var successCallback = function(){
					var shared = "%" + auth + "%";

					mysql_bhsUser.query("UPDATE bhs_form_templates SET `form_title` = ?, `_raw_html` = ?, `_formatted` = ? WHERE BINARY `_doc_token` = ? and `_delete` = 0 and `_shared_arr` like ?", [reqData["commit"]["title"], reqData["commit"]["raw"], reqData["commit"]["formatted"], reqData["commit"]["token"], shared], function (error, results, fields) {
						if(error){ 
							console.log(error);
							responseData["message"] = "Oops! Unable to update form, please try again later.";
							res.send(responseData);
						}
						else{ 
							mysql_bhsUser.query("INSERT INTO `bhs_form_history`(`_history_author`, `_history_event`, `_history_stash`, `_history_commit`, `_history_doc`) VALUES (?, ?, ?, ?, ?)", [auth, "onShareForm",JSON.stringify(reqData["stash"]), JSON.stringify(reqData["commit"]), reqData["commit"]["token"]], function (error, results, fields) {
								if(error){
									console.log(reqData["commit"]["token"]);
									console.log(error);
									responseData["message"] = "Oops! Unable to update history. Please try again later.";
								}
								else{
									responseData["message"] = "Form template updated successfully."; 
									responseData["data"] = 200;
								}

								res.send(responseData);
							});
						}
					});
				}

				var errorCallback = function(){
					responseData["message"] = "Oops! Invalid authentication key."; 
					res.send(responseData);
				}

				validateAuth(auth, reqData, responseData, res, successCallback, errorCallback);
			});

		// share form
		// cases:
			// update share trigger activity history (in: auth (as author), shared arr, form id | out: 200)
			express.post("/bhs-lab-server/user-shareUnshareForm", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				var successCallback = function(){
					reqData["arr"] = JSON.stringify(reqData["arr"]);

					mysql_bhsUser.query("UPDATE bhs_form_templates SET `_shared_arr` = ? WHERE `_doc_token` = ?", [reqData["arr"], reqData["form_id"]], function (error, results, fields) {
						if(error){ 
							console.log(error);
							responseData["message"] = "Oops! Unable to share form, please try again later.";
							res.send(responseData);
						}
						else{ 
							mysql_bhsUser.query("INSERT INTO `bhs_form_history`(`_history_author`, `_history_event`, `_history_share`, `_history_doc`) VALUES (?, ?, ?, ?)", [auth, "onShareForm", reqData["arr"], reqData["form_id"]], function (error, results, fields) {
								if(error){
									console.log(error);
									responseData["message"] = "Oops! Unable to update history. Please try again later.";
								}
								else{
									responseData["message"] = "Form shared to your circle."; 
									responseData["data"] = 200;
								}

								res.send(responseData);
							});
						}
					});
				}

				var errorCallback = function(){
					responseData["message"] = "Oops! Invalid authentication key."; 
					res.send(responseData);
				}

				validateAuth(auth, reqData, responseData, res, successCallback, errorCallback);
			});

		// delete form
		// cases: 
			// update _delete status of form (in: auth (as author), form id | out: 200)
			express.post("/bhs-lab-server/user-deleteForm", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				var successCallback = function(){
					mysql_bhsUser.query("UPDATE bhs_form_templates SET `_delete` = true WHERE `_doc_token` = ?", [reqData["form_id"]], function (error, results, fields) {
						if(error){ 
							console.log(error);
							responseData["message"] = "Oops! Unable to delete form, please try again later.";
							res.send(responseData);
						}
						else{ 
							mysql_bhsUser.query("INSERT INTO `bhs_form_history`(`_history_author`, `_history_event`, `_history_doc`) VALUES (?, ?, ?)", [auth, "onDeleteForm", reqData["form_id"]], function (error, results, fields) {
								if(error){
									console.log(error);
									responseData["message"] = "Oops! Unable to update history. Please try again later.";
								}
								else{
									responseData["message"] = "Form successfully deleted."; 
									responseData["data"] = 200;
								}

								res.send(responseData);
							});
						}
					});
				}

				var errorCallback = function(){
					responseData["message"] = "Oops! Invalid authentication key."; 
					res.send(responseData);
				}

				validateAuth(auth, reqData, responseData, res, successCallback, errorCallback);
			});

		// get form/s
		// cases:
			// get one form for download, update or etc (in: auth, form id | out: form)
			// get all forms related (shared and authored) to user (in: auth (as author) | out: forms)
			express.get("/bhs-lab-server/user-pdf-form", (req, res) => {
				var responseData = { "message": null, "data": null };

				var reqData = req.query;

				var queryString = "SELECT `_formatted` as 'template' FROM `bhs_form_templates` WHERE BINARY `_delete` = 0 and `_doc_token` = ? limit 1";

				mysql_bhsUser.query(queryString, [reqData["form_id"]], function (error, results, fields) {
					if(error){ 
						console.log(error);
						responseData["message"] = "Oops! Unable to get forms, please try again later.";
					}
					else{ 
						responseData["message"] = "Form/s list found."; 
						responseData["data"] = results;
					}

					res.send(responseData);
				});
			});

	// RESPONSE----------------------------------------------
		// get response/s
		// cases:
			// get one response for download, update or etc (in: auth, response id | out: response)
			// get all response of all forms (shared and authored) related to this user (in: auth (as author) | out: responses)
			express.get("/bhs-lab-server/user-getAllSubmissions", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.query;

				var successCallback = function(){
					var queryString = "SELECT b.`form_title` as 'title', a.`_form_token` as 'form', a.`_raw_html_withAns` as 'raw', a.`_formatted_withAns` as 'formatted', a.`_response_token` as 'response_id', c.`bhs_email` as 'respondent', DATE_FORMAT(a.`_date_time`, '%m-%d-%Y %h:%m %p') as 'time' FROM `bhs_form_response` a, `bhs_form_templates` b, bhs_admin.bhs_users c WHERE BINARY a.`_form_token` = b.`_doc_token` and a.`_respondent_token` = c.`_token` and a.`_delete` = 0 and (a.`_form_token` in (SELECT `_doc_token` FROM `bhs_form_templates` WHERE BINARY `_delete` = 0 and `_author` = ? or `_shared_arr` like ?) or a.`_respondent_token` = ?)";
					var queryValues = [auth, "%" + auth + "%", auth];

					if(reqData["response_id"] != ""){ 
						queryString += " and a.`_response_token` = ?";
						queryValues.push(reqData["response_id"]);
					}

					queryString += " order by a.`_date_time` desc";

					mysql_bhsUser.query(queryString, queryValues, function (error, results, fields) {
						if(error){ 
							console.log(error);
							responseData["message"] = "Oops! Unable to get responses, please try again later.";
						}
						else{ 
							responseData["message"] = "Response/s list found."; 
							responseData["data"] = results;
						}

						res.send(responseData);
					});
				}

				var errorCallback = function(){
					responseData["message"] = "Oops! Invalid authentication key."; 
					res.send(responseData);
				}

				validateAuth(auth, reqData, responseData, res, successCallback, errorCallback);
			});

		// create response
		// cases: 
			// create new response and trigger activity history (in: auth (as author), form id | out: 200)
			express.post("/bhs-lab-server/user-createResponseOnForm", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				var successCallback = function(){
					var response_token = rand() + rand();
					mysql_bhsUser.query("INSERT INTO `bhs_form_response`(`_form_token`, `_respondent_token`, `_response_token`) VALUES (?, ?, ?)", [reqData["form_id"], auth, response_token], function (error, results, fields) {
						if(error){ 
							console.log(error);
							responseData["message"] = "Oops! Unable to create response, please try again later.";
							res.send(responseData);
						}
						else{ 
							mysql_bhsUser.query("INSERT INTO `bhs_form_history`(`_history_author`, `_history_event`, `_history_doc`) VALUES (?, ?, ?)", [auth, "onResponse", reqData["form_id"]], function (error, results, fields) {
								if(error){
									console.log(error);
									responseData["message"] = "Oops! Unable to update history. Please try again later.";
								}
								else{
									console.log(results);
									responseData["message"] = "Response created, wait or open the builder."; 
									responseData["data"] = response_token;
								}

								res.send(responseData);
							});
						}
					});
				}

				var errorCallback = function(){
					responseData["message"] = "Oops! Invalid authentication key."; 
					res.send(responseData);
				}

				validateAuth(auth, reqData, responseData, res, successCallback, errorCallback);
			});

		// update response content
		// cases:
			// update response and trigger activity history (in: auth (as author), response content, response id | out: 200)
			express.post("/bhs-lab-server/user-updateResponse", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				var successCallback = function(){
					// allow everyone with shared access to edit this response
					mysql_bhsUser.query("UPDATE bhs_form_response SET `_raw_html_withAns` = ?, `_formatted_withAns` = ? WHERE BINARY `_delete` = 0 and `_response_token` = ?", [reqData["commit"]["raw"], JSON.stringify(reqData["commit"]["formatted"]), reqData["stash"]["response_id"]], function (error, results, fields) {
						if(error){ 
							console.log(error);
							responseData["message"] = "Oops! Unable to update response, please try again later.";
							res.send(responseData);
						}
						else{ 
							mysql_bhsUser.query("INSERT INTO `bhs_form_history`(`_history_author`, `_history_event`, `_history_doc`, `_history_stash`, `_history_commit`) VALUES (?, ?, ?, ?, ?)", [auth, "onUpdateResponse", reqData["stash"]["form"], JSON.stringify(reqData["stash"]), JSON.stringify(reqData["commit"])], function (error, results, fields) {
								if(error){
									console.log(error);
									responseData["message"] = "Oops! Unable to update history. Please try again later.";
								}
								else{
									responseData["message"] = "Response successfully updated."; 
									responseData["data"] = 200;
								}

								res.send(responseData);
							});
						}
					});
				}

				var errorCallback = function(){
					responseData["message"] = "Oops! Invalid authentication key."; 
					res.send(responseData);
				}

				validateAuth(auth, reqData, responseData, res, successCallback, errorCallback);
			});

		// delete response
		// cases: 
			// update _delete status of response (in: auth (as author), response id | out: 200)
			express.post("/bhs-lab-server/user-deleteResponse", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				var successCallback = function(){
					mysql_bhsUser.query("UPDATE bhs_form_response SET `_delete` = true WHERE `_response_token` = ?", [reqData["response_id"]], function (error, results, fields) {
						if(error){ 
							console.log(error);
							responseData["message"] = "Oops! Unable to delete response, please try again later.";
							res.send(responseData);
						}
						else{ 
							mysql_bhsUser.query("INSERT INTO `bhs_form_history`(`_history_author`, `_history_event`, `_history_doc`) VALUES (?, ?, ?)", [auth, "onDeleteResponse", reqData["form_id"]], function (error, results, fields) {
								if(error){
									console.log(error);
									responseData["message"] = "Oops! Unable to update history. Please try again later.";
								}
								else{
									responseData["message"] = "Response successfully deleted."; 
									responseData["data"] = 200;
								}

								res.send(responseData);
							});
						}
					});
				}

				var errorCallback = function(){
					responseData["message"] = "Oops! Invalid authentication key."; 
					res.send(responseData);
				}

				validateAuth(auth, reqData, responseData, res, successCallback, errorCallback);
			});

		// parse to PDF Response/s
		// cases:
			express.get("/bhs-lab-server/user-pdf-response", (req, res) => {
				var responseData = { "message": null, "data": null };

				var reqData = req.query;

				var queryString = "SELECT b.`form_title` as 'title', a.`_form_token` as 'token', a.`_formatted_withAns` as 'response', c.`bhs_email` as 'respondent', DATE_FORMAT(a.`_date_time`, '%m-%d-%Y %h:%m %p') as 'time' FROM `bhs_form_response` a, `bhs_form_templates` b, bhs_admin.bhs_users c WHERE BINARY a.`_form_token` = b.`_doc_token` and a.`_respondent_token` = c.`_token` and a.`_delete` = 0 and a.`_response_token` = ? limit 1";
				var queryValues = [reqData["response_id"]];

				mysql_bhsUser.query(queryString, queryValues, function (error, results, fields) {
					if(error){ 
						console.log(error);
						responseData["message"] = "Oops! Unable to get responses, please try again later.";
					}
					else{ 
						responseData["message"] = "Response found."; 
						responseData["data"] = results;
					}

					res.send(responseData);
				});
			});
// --------------------------------------------------------



// Proper error handling
	express.use(function(req, res){ res.status(400).sendFile(reqPath.join(__dirname + "/../frontend/404.html"));});
	express.use(function(error, req, res) { 
		console.log(error);
		res.status(500).sendFile(reqPath.join(__dirname + "/../frontend/500.html"));
	});
// --------------------------------------------------------




// default instance
	express.listen(config["default-port"], () => { 
		console.log("listening to port " + config["default-port"]);
		mysql_bhsAdmin.connect(function(err){
			if(err){ console.error("error connecting [mysql - bhs_admin]: " + err.stack); }
			else{ 
				console.log("connected as id " + mysql_bhsAdmin.threadId); 
				setInterval(function () { mysql_bhsAdmin.query('SELECT 1'); }, 3000);
			}
		});
		mysql_bhsUser.connect(function(err){
			if(err){ console.error("error connecting [mysql - bhs_user]: " + err.stack); }
			else{ 
				console.log("connected as id " + mysql_bhsUser.threadId); 
				setInterval(function () { mysql_bhsUser.query('SELECT 1'); }, 500);
			}
		});
	});
// --------------------------------------------------------



// NOTES, LINKS and REFERENCES
	// date and time format!
	// https://www.w3schools.com/sql/func_date_format.asp