
// default data
	const config = JSON.parse(conf);
	const bhsuser_global_server = config["protocol"] + "://" + config["address"] + ":" + config["port"];

// Editor
	var editor = grapesjs.init({
		container : "#container-grapejs",
		// disable scripts - jquery in bootstrap will dirupt grapesjs jquery
		allowScripts: 0,
		height: "100%",
		// load components after init
		components: '',
		// load styles after init
		style: '',
		// disregard DOM contents in the before init
		fromElement: false,
		panels: {
			defaults: [{
				// leave empty, this is center aligned selections
				id: "commands",
				buttons : [{}]
			}, {
				id: "options",
				buttons : [{
					id: 'save-form',
					className: 'fa fa-save',
					command: 'saveTemplate'
				}, {
					id: 'return-home',
					className: 'fa fa-home',
					command: 'goHome'
				}]
			}, {
				id: 'views',
				buttons: [{
					id: 'open-block-manager',
					className: 'fa fa-edit',
					command: 'open-blocks',
					active: true
				}, {
					id: 'open-trait-manager',
					className: 'fa fa-cog',
					command: 'open-tm'
				}]
			}]
		},
		blockManager: {
			blocks: [{
				label: "Title",
				content: '<h3>Title</h3><hr>',
				attributes: { class: "fa fa-font" }
			}, {
				label: "Text",
				content: '<div class="form-group"><label class="control-label">Input Label</label><input class="form-control" type="text"></div>',
				attributes: { class: "fa fa-text-width" }
			}, {
				label: "Date",
				content: '<div class="form-group"><label class="control-label">Date</label><input class="form-control" type="date"></div>',
				attributes: { class: "fa fa-calendar" }
			}, {
				label: "Numeric",
				content: '<div class="form-group"><label class="control-label">Number</label><input class="form-control" type="number"></div>',
				attributes: { class: "fa fa-sort-numeric-asc" }
			}, {
				label: "Email",
				content: '<div class="form-group"><label class="control-label">Email</label><input class="form-control" type="email"></div>',
				attributes: { class: "fa fa-envelope-o" }
			}, {
				label: "Image Link",
				content: '<div class="form-group"><label class="control-label">Image Link</label><input class="form-control" type="url" todo="onblur:showPreview(this);" placeholder="Insert Image URL"><div class="show_img_here"></div></div>',
				attributes: { class: "fa fa-link" }
			}, {
				label: "Checkbox",
				content: '<div class="row">\
							<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"><input type="checkbox"> <span>Checkbox label</span></div>\
							<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"><input type="checkbox"> <span>Checkbox label</span></div>\
						</div>',
				attributes: { class: "fa fa-check-square-o" }
			}, {
				label: "Radio Button",
				content: '<form>\
							<div class="row">\
								<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"><input type="radio" name="radioSelect"> <div style="display:inline-block">Radio label</div></div>\
								<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"><input type="radio" name="radioSelect"> <div style="display:inline-block">Radio label</div></div>\
							</div>\
						</form>',
				attributes: { class: "fa fa-dot-circle-o" }
			}, {
				label: "Long Text",
				content: '<div class="form-group"><label class="control-label">Text Area</label><textarea class="form-control" rows="3"></textarea></div>',
				attributes: { class: "fa fa-align-justify" }
			}, {
				label: "Static Text",
				content: '<div class="form-group"><label class="control-label">Static Text</label><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></div>',
				attributes: { class: "fa fa-list" }
			}, {
				label: "Colony Count / m3",
				content: '<div class="form-group"><label class="control-label">Colony Count</label><input class="form-control" type="text" todo="onblur:colonyCount_pcm(this);" placeholder="Input Colony Count for computation"></div>',
				attributes: { class: "fa fa-plus-square" }
			}, {
				label: "<small>- Group -</small> Client Identification",
				content: '<div class="form-group"><label class="control-label">Client Name</label><input class="form-control" type="text"></div>\
					<div class="form-group"><label class="control-label">Client Address</label><input class="form-control" type="text"></div>\
					<div class="form-group"><label class="control-label">Client Email</label><input class="form-control" type="email"></div>\
					<div class="form-group"><label class="control-label">Client Telephone Number</label><input class="form-control" type="text"></div>\
					<div class="form-group"><label class="control-label">Invoice Number</label><input class="form-control" type="text"></div>\
					<div class="form-group"><label class="control-label">Project Initials</label><input class="form-control" type="text"></div>\
					<div class="form-group"><label class="control-label">Date of this Lab Report</label><input class="form-control" type="date"></div>',
				attributes: { class: "fa fa-plus-square" }
			}, {
				label: "<small>- Group -</small> Species Types",
				content: '<div class="form-group"><label class="control-label">Species Types</label>\
							<select multiple="multiple" class="form-control">\
							<option class=\"text-muted\">Choose One or More</option>\
							<option>Absidia</option>\
							<option>Acremonium</option>\
							<option>Alternaria</option>\
							<option>Arthrinium</option>\
							<option>Aureobasidium pullulans</option>\
							<option>Aspergillus calidoustus</option>\
							<option>Aspergillus flavus</option>\
							<option>Aspergillus fumigatus</option>\
							<option>Aspergillum glaucus complex</option>\
							<option>Aspergillus nidulans</option>\
							<option>Aspergillus niger</option>\
							<option>Aspergillus ochraceous</option>\
							<option>Aspergillus sclerotium</option>\
							<option>Aspergillus sydowii</option>\
							<option>Aspergillus terreus</option>\
							<option>Aspergillus ustus complex</option>\
							<option>Aspergillus versicolor</option>\
							<option>Aureobasidium</option>\
							<option>Beauveria</option>\
							<option>Bipolaris</option>\
							<option>Candida sp.</option>\
							<option>Chaetomium</option>\
							<option>Chrysosporium</option>\
							<option>Cladosporium</option>\
							<option>Coccidioides</option>\
							<option>Curvularia</option>\
							<option>Dreschslera australiensis</option>\
							<option>Eurotium amstelodami</option>\
							<option>Epicoccum</option>\
							<option>Emericella nidulans</option>\
							<option>Emericella unguis</option>\
							<option>Eurotium</option>\
							<option>Exserohilum</option>\
							<option>Fonsecea</option>\
							<option>Fusarium</option>\
							<option>Geotrichum candidum</option>\
							<option>Gliocladium</option>\
							<option>Malbranchea</option>\
							<option>Memnoniella echinata</option>\
							<option>Microsporum sp.</option>\
							<option>Mucor</option>\
							<option>Neoscytalidium</option>\
							<option>Nigrospora</option>\
							<option>Paecilomyces</option>\
							<option>Penicillium sp. (Genus only)</option>\
							<option>Penicillium brevicompactum</option>\
							<option>Penicillium chrysogenum</option>\
							<option>Penicillium corylophilum</option>\
							<option>Penicillium crustosum</option>\
							<option>Penicillium marneffei (Risk Group 2 fungal agent)</option>\
							<option>Penicillium purpogenum</option>\
							<option>Paecilomyces variotii</option>\
							<option>Phialophora</option>\
							<option>Phycomyces</option>\
							<option>Phoma</option>\
							<option>Pithomyces</option>\
							<option>Rhizomucor</option>\
							<option>Rhizopus</option>\
							<option>Rhodotorula</option>\
							<option>Sarcocladium strictum</option>\
							<option>Scedosporium</option>\
							<option>Scytalidium</option>\
							<option>Scopulariopsis</option>\
							<option>Sporothrix sp.</option>\
							<option>Stachybotrys</option>\
							<option>Syncephalastrum</option>\
							<option>Trichoderma sp. (Genus only)</option>\
							<option>Trichoderma utroviride</option>\
							<option>Trichoderma virens</option>\
							<option>Trichophyton sp.</option>\
							<option>Ulocladium</option>\
							<option>Unidentified Yeast</option>\
							<option>Unidentified Fungus</option>\
							<option>Bacteria</option>\
							<option>NIL GROWTH</option>\
							</select>\
						</div>',
				attributes: { class: "fa fa-plus-square" }
			}, {
				label: "<small>- Group -</small> Hygiene Rating",
				content: '<div class="form-group"><label class="control-label">Hygiene Rating Scale Assessment</label>\
							<select class="form-control">\
							<option class=\"text-muted\">Choose One</option>\
							<option>LOW Hygiene = < 12 CFU/Plate</option>\
							<option>NORMAL Hygiene = 13-28 CFU/Plate</option>\
							<option>ELEVATED Hygiene = 29-57 CFU/Plate + prevailing species</option>\
							<option>HIGH (contaminated) Hygiene = 58-150 CFU/Plate + dominant species</option>\
							<option>EXTREMELY HIGH (contaminated) Hygiene = 151-300+ CFU/Plate + dominant species + confluent growth</option>\
							</select>\
						</div>',
				attributes: { class: "fa fa-plus-square" }
			}, {
				label: "<small>- Group -</small> Risk Rating",
				content: '<div class="form-group"><label class="control-label">Risk Rating Scale Assessment</label>\
							<select class="form-control">\
							<option class=\"text-muted\">Choose One</option>\
							<option>Not Appropriate</option>\
							<option>NORMAL Risk = ? Outdoor Air (OA) Control</option>\
							<option>ELEVATED RISK = > OA to ? 2 x OA (i.e greater than the outdoor control but less than twice the number of CFU seen for the control)</option>\
							<option>AT RISK = >2 x OA</option>\
							<option>HAZARDOUS RISK  = >2 x OA + speciation</option>\
							<option>SEVERE RISK = > 2 x OA + speciation + known mycotoxins</option>\
							</select>\
						</div>',
				attributes: { class: "fa fa-plus-square" }
			}, {
				label: "<small>- Group -</small> Test Kit Type",
				content: '<div class="form-group"><label class="control-label">Test Kit Type</label>\
							<select class="form-control">\
							<option class=\"text-muted\">Choose One</option>\
							<option>2-Sample Mould Test Kit</option>\
							<option>3-Sample Mould Test Kit</option>\
							<option>4-Sample Mould Test Kit</option>\
							<option>5-Sample Mould Test Kit</option>\
							<option>6-Sample Mould Test Kit</option>\
							<option>7-Sample Mould Test Kit</option>\
							<option>8-Sample Mould Test Kit</option>\
							<option>Bio-Tape Slides</option>\
							<option>Sticky Tape</option>\
							</select>\
						</div>',
				attributes: { class: "fa fa-plus-square" }
			}, {
				label: "<small>- Group -</small> Test Kit Type - MULTIPLE",
				content: '<div class="form-group"><label class="control-label">Test Kit Type</label>\
							<select multiple="multiple" class="form-control">\
							<option class=\"text-muted\">Choose One or More</option>\
							<option>2-Sample Mould Test Kit</option>\
							<option>3-Sample Mould Test Kit</option>\
							<option>4-Sample Mould Test Kit</option>\
							<option>5-Sample Mould Test Kit</option>\
							<option>6-Sample Mould Test Kit</option>\
							<option>7-Sample Mould Test Kit</option>\
							<option>8-Sample Mould Test Kit</option>\
							<option>User Submitted Sample</option>\
							<option>Lab Submitted or Sampled by BHS</option>\
							<option>Multiple Sets (x2)</option>\
							<option>Multiple Sets (x3)</option>\
							<option>Multiple Sets (x4)</option>\
							</select>\
						</div>',
				attributes: { class: "fa fa-plus-square" }
			}, {
				label: "<small>- Group -</small> Type of Test",
				content: '<div class="form-group"><label class="control-label">Type of Test</label>\
							<select multiple="multiple" class="form-control">\
							<option class=\"text-muted\">Choose One</option>\
							<option>Spore Trap</option>\
							<option>Air Sample</option>\
							<option>Spore Concentration</option>\
							<option>Spore Type Identification</option>\
							<option>Pre-Remediation</option>\
							<option>Post-Remediation Verification</option>\
							<option>During Remediation</option>\
							</select>\
						</div>',
				attributes: { class: "fa fa-plus-square" }
			}, {
				label: "<small>- Group -</small> Yes-No-NA",
				content: '<div class="form-group"><label class="control-label">Yes/No/NA Option</label>\
							<select class="form-control">\
							<option class=\"text-muted\">NA / Yes / No</option>\
							<option>NA</option>\
							<option>Yes</option>\
							<option>No</option>\
							</select>\
						</div>',
				attributes: { class: "fa fa-plus-square" }
			}, {
				label: "<small>- Group -</small> Air Surface Choice",
				content: '<div class="form-group"><label class="control-label">Air Surface Choice</label>\
							<select class="form-control">\
							<option class=\"text-muted\">Choose One</option>\
							<option>Air Sample</option>\
							<option>Surface Swab</option>\
							</select>\
						</div>',
				attributes: { class: "fa fa-plus-square" }
			}]
		},
		storageManager: {
			id: 'gjs-',
			type: 'local',
			autosave: true,
			autoload: true,
			stepsBeforeSave: 1,
			storeComponents: true,
			storeStyles: true,
			storeHtml: true,
			storeCss: true
		},
		assetManager: {
			assets: [],
			upload: 0,
			uploadText: 'Uploading is not available in this demo',
		},
		traitManager: { labelContainer: "Input IDs" },
		// functions cast in the builder
		commands: {
			defaults: [{
				id: "saveTemplate",
				run: function(editor, senderBtn){
					editor.store();

					// SAVE FORM TEMPLATE --------------------------
						// secure copy of current stagedBuilder
						var stash = JSON.parse(localStorage.getItem("stagedBuilder"));

						// assign stagedBuilder for editing and submission
						var commit = JSON.parse(localStorage.getItem("stagedBuilder"));

						// edit template title here...
						// commit["title"] = $("#formTitle").val();

						commit["raw"] = JSON.stringify({
							components: localStorage.getItem("gjs-components"),
							styles: localStorage.getItem("gjs-styles") 
						});
						commit["formatted"] = JSON.stringify({
							// do not store assets, there are no images to attach to begin with
							// assets: localStorage.getItem("gjs-assets"),
							html: localStorage.getItem("gjs-html"),
							// do not store css, a default css is loaded on init
							// css: localStorage.getItem("gjs-css") 
						});

						// send through rest api...
						$.ajax({
							method: "post",
							url: bhsuser_global_server + "/bhs-lab-server/user-updateForm",
							contentType: "application/json",
							headers: { "Authorization": JSON.parse(localStorage.getItem("bhs-user"))["_token"] },
							data: JSON.stringify({ stash: stash, commit: commit })
						})
						.done(function(data, textStatus, xhr){
							// console.log(data);
							// console.log(textStatus);
							// console.log(xhr);

							alert(data["message"]);
							if(data["data"] == 200){
								localStorage.setItem("stagedBuilder", JSON.stringify(commit));
								// console.log(JSON.parse(localStorage.getItem("stagedBuilder")));
							}
						});
					// ---------------------------------------------

					senderBtn.set("active", false);
				},
				stop: function(editor, senderBtn){},
			}, {
				id: "goHome",
				run: function(editor, senderBtn){
					localStorage.removeItem("stagedBuilder");
					window.location.replace("/bhs-lab");
					senderBtn.set("active", false);
				},
				stop: function(editor, senderBtn){},
			}]
		}
	});

// update traits - Assign a unique id for all input elements
	function customTraits(){
		var comps = editor.DomComponents;

		// Get the model and the view from the default Component type
		var defaultType = comps.getType('default');
		var defaultModel = defaultType.model;
		var defaultView = defaultType.view;

		// input fields should have unique id
		var inputField_count = 1;
		comps.addType('Input-Fields', {
			model: defaultModel.extend(
				{ defaults: 
					Object.assign({}, defaultModel.prototype.defaults, { droppable: false, traits: ['id'] }),
					init: function() {
						try{ 
							if(this.get('attributes')["type"] == undefined){ 
								var unique_id = this.attributes.tagName;
							}
							else{ 
								var unique_id = this.get('attributes')["type"];
							}
						}
						catch(Error){ var unique_id = this.attributes.tagName; }

						if(isNaN(inputField_count)){ inputField_count = (Math.floor(Math.random() * (1000000 - 99)) + 99); }

						unique_id = unique_id + "-" + (new Date().getTime()) + "-" + inputField_count;

						var currentAttr = this.get('attributes');

						// check if this id is ALWAYS unique or else inc count and make new id

						try{
							// if init but is empty | undefinded | null
							if (this.get('attributes')["id"] == null || this.get('attributes')["id"].trim() == "" || this.get('attributes')["id"] == 'undefined' || this.get('attributes')["id"] === undefined) { 
								currentAttr["id"] = unique_id;
								this.set('attributes', currentAttr);
							}
							else{ 
								// check if id is valid-format
								partsCount = this.get('attributes')["id"].split("-");
								if(isNaN(parseInt(partsCount[2]))){ 
									inputField_count = (Math.floor(Math.random() * (1000000 - 99)) + 99);
									currentAttr["id"] = partsCount[0] + "-" + partsCount[1] + "-" + inputField_count;
									this.set('attributes', currentAttr);
								}
								else{ inputField_count = parseInt(this.get('attributes')["id"].split("-")[2]);  }
							}
						}
						catch(err){ 
							// attribute not exist
							// this.set('attributes', { id: "asd" });
							currentAttr["id"] = unique_id;
							this.set('attributes', currentAttr);
						}

						inputField_count++;
					}
				}, {
					isComponent: function(el) { 
						if(el.tagName == 'INPUT' || el.tagName == 'SELECT' || el.tagName == 'TEXTAREA'){ return { type: 'Input-Fields' }; } 
					}
				}
			), view: defaultType.view,
		});

		// disable dropping elements outside pf designated frame
		comps.addType('Frame', {
			model: defaultModel.extend(
				{ defaults: Object.assign({}, defaultModel.prototype.defaults, { droppable: false, traits: [] })}, 
				{ 
					isComponent: function(el) { 
						try{ if(el.getAttribute("name") == 'outerFrame'){ return { type: 'Frame' }; } }
						catch(e){ console.log("- text - element - no getAttribute() function -"); }
					} 
				}
			), view: defaultType.view,
		});
	}
	customTraits();

// components + styles loader
	function loadComponents(components){ 
		editor.setComponents(components);

		// adding js files only work if allowScripts is enabled in editor -- do not add a js - it may distupt grapes' js
		// if(components.indexOf("1.12.4/jquery.min.js\"></script>") == -1){
		// 	editor.getComponents().add("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>");
		// }

		// if(components.indexOf("3.3.7/js/bootstrap.min.js\"></script>") == -1){
		// 	editor.getComponents().add("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>");
		// }

		var inlineRadio = "<link rel=\"stylesheet\" href=\"css/builder2.css\">";
		if(components.indexOf("href=\"css/builder2.css\"") == -1){
			editor.getComponents().add(inlineRadio);
		}
	}
	function loadStyle(styles){ 
		editor.setStyle(styles);
	}

// default components
// check if there is already an existing "formatted" template and load it
	var tempFormatted = JSON.parse(localStorage.getItem("stagedBuilder"))["formatted"];
	var default_form = "";
	if(tempFormatted.trim() != ""){ default_form = JSON.parse(tempFormatted)["html"]; }
	else{ 
		default_form = "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">";
		default_form += "<link rel=\"stylesheet\" href=\"https://bootswatch.com/paper/bootstrap.css\">";
		default_form += "<link rel=\"stylesheet\" href=\"css/builder2.css\">";
		default_form += "<div name=\"outerFrame\" class=\"outerFrame\"><div class=\"innerFrame\"></div></div>";
	}
	loadComponents(default_form);

	var default_css = '\
		.outerFrame{ \
			padding: 20px 15% 15px 15%;\
			background-color:#EAECED;\
			width:100%;\
		}\
		.innerFrame {\
			background-color: #fff;\
			width: 100%;\
			min-height: 100px;\
			padding: 10px;\
		}';
	loadStyle(default_css);

// Simple warn notifier
	var origWarn = console.warn;
	console.warn = function (msg) {
		alert(msg);
		origWarn(msg);
	};

// if user is trying to leave the page
	function clearCommon(){
		localStorage.removeItem("gjs-assets");
		localStorage.removeItem("gjs-components");
		localStorage.removeItem("gjs-css");
		localStorage.removeItem("gjs-html");
		localStorage.removeItem("gjs-styles");
	}
	$(window).bind("beforeunload", function(event) {
		// clear the localstoage for editor elements
		clearCommon();
		return "Make sure you have saved your changes";
	});
