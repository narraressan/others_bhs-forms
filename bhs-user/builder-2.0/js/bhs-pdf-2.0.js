
// default data
	const config = JSON.parse(conf);
	const bhsuser_global_server = config["protocol"] + "://" + config["address"] + ":" + config["port"];

// Editor
	var editor = grapesjs.init({
		container : "#container-grapejs",
		height: "100%",
		// load components after init
		components: '',
		// load styles after init
		style: '',
		// disregard DOM contents in the before init
		fromElement: false,
		panels: {
			defaults: [{
				// leave empty, this is center aligned selections
				id: "commands",
				buttons : [{}]
			}, {
				id: "options",
				buttons : [{
					id: 'save-form',
					className: 'fa fa-save',
					command: 'saveTemplate'
				}, {
					id: 'return-home',
					className: 'fa fa-home',
					command: 'goHome'
				}]
			}, {
				id: 'views',
				buttons: [{
					id: 'open-block-manager',
					className: 'fa fa-edit',
					command: 'open-blocks',
					active: true
				}, {
					id: 'open-trait-manager',
					className: 'fa fa-cog',
					command: 'open-tm'
				}]
			}]
		},
		blockManager: {
			blocks: [{
				label: "Title",
				content: '<h4>Title</h4><hr>',
				attributes: { class: "fa fa-font" }
			}, {
				label: "Text",
				content: '<label><small>Text</small></label><input type="text" class="u-full-width">',
				attributes: { class: "fa fa-text-width" }
			}, {
				label: "Date",
				content: '<label><small>Date</small></label><input type="date" class="u-full-width">',
				attributes: { class: "fa fa-calendar" }
			}, {
				label: "Numeric",
				content: '<label><small>Number</small></label><input type="number" class="u-full-width">',
				attributes: { class: "fa fa-sort-numeric-asc" }
			}, {
				label: "Email",
				content: '<label><small>Email</small></label><input type="email" class="u-full-width">',
				attributes: { class: "fa fa-envelope-o" }
			}, {
				label: "Image Link",
				content: '<label><small>Image Link</small></label><input type="url" class="u-full-width" todo="onblur:showPreview(this);" placeholder="Insert Image URL"><small><!--show image here...--></small>',
				attributes: { class: "fa fa-link" }
			}, {
				label: "Checkbox",
				content: '<div class="row"><div class="twelve columns"><input type="checkbox"><span class="label-body"><small>Checkbox Label</small></span></div></div>',
				attributes: { class: "fa fa-check-square-o" }
			}, {
				label: "Radio Button",
				content: '<form><label><small>Radio</small></label>\
					<div><input type="radio" name="radio"><span class="label-body"><small>Radio Label</small></span></div>\
					<div><input type="radio" name="radio"><span class="label-body"><small>Radio Label</small></span></div></form>',
				attributes: { class: "fa fa-dot-circle-o" }
			}, {
				label: "Long Text",
				content: '<label><small>Text Area</small></label><textarea class="u-full-width" rows="10"></textarea>',
				attributes: { class: "fa fa-align-justify" }
			}, {
				label: "Static Text",
				content: '<label><small>Static Text</small></label><p class="static-texts">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
				attributes: { class: "fa fa-list" }
			}, {
				label: "Colony Count / m3",
				content: '<label><small>Colony Count</small></label><input type="url" class="u-full-width" todo="onblur:colonyCount_pcm(this);" placeholder="Input Colony Count for computation">',
				attributes: { class: "fa fa-plus-square" }
			}]
		},
		storageManager: {
			id: 'gjs-',
			type: 'local',
			autosave: true,
			autoload: true,
			stepsBeforeSave: 1,
			storeComponents: true,
			storeStyles: true,
			storeHtml: true,
			storeCss: true
		},
		assetManager: {
			assets: [],
			upload: 0,
			uploadText: 'Uploading is not available in this demo',
		},
		traitManager: { labelContainer: "Input IDs" },
		// functions cast in the builder
		commands: {
			defaults: [{
				id: "saveTemplate",
				run: function(editor, senderBtn){
					editor.store();
					console.log("save");
					senderBtn.set("active", false);
				},
				stop: function(editor, senderBtn){},
			}, {
				id: "goHome",
				run: function(editor, senderBtn){
					console.log("home");
					senderBtn.set("active", false);
				},
				stop: function(editor, senderBtn){},
			}]
		}
	});

// components + styles loader
	function loadComponents(components){ 
		editor.setComponents(components); 
	}

// Simple warn notifier
	var origWarn = console.warn;
	console.warn = function (msg) {
		alert(msg);
		origWarn(msg);
	};

// if user is trying to leave the page
	function clearCommon(){
		localStorage.removeItem("gjs-assets");
		localStorage.removeItem("gjs-components");
		localStorage.removeItem("gjs-css");
		localStorage.removeItem("gjs-html");
		localStorage.removeItem("gjs-styles");
	}
	$(window).bind("beforeunload", function(event) {
		// clear the localstoage for editor elements
		clearCommon();
		return "Make sure you have saved your changes";
	});
