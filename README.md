
SERVER SETUP
- Ubuntu 16.04.01 - server

default setup
- sudo apt-get update
- sudo apt-get install mysql-server 
	- pass: bhsroot
	- mysql -u username -p bhs_admin < bhs_admin.sql
	- mysql -u username -p bhs_user < bhs_user.sql
	- sudo nano /etc/mysql/mysql.cnf
		[mysqld]
		sql-mode = ""
- sudo apt-get install nodejs-legacy
	- node --version
- sudo apt-get install npm
	- npm
- sudo apt-get install git
	- git --version
- cd ..
- sudo mkdir bhs-running-teamNarra
- cd bhs-running-teamNarra
- git init
- sudo git clone https://github.com/narraressan/bhs-forms.git
- sudo npm install nodemon -g
- cd bhs-forms
- sudo npm install
- sudo apt-get install mongodb

<!-- Note for GALLERY -->
- make sure you have 'gallery' inside 'bhs-use/builder'

<!-- initialize mongodb -- MONGODB USAGE DISABLED SINCE FEB 25, 2017 -- unfelxible query>
<!-- windows: -->
	+ create C:\data\db
	+ run mongod
	+ run mongo.exe
	+ show dbs;
	+ use bhslab_user;
	+ db.createCollection("bhs_user_attendance")
	+ db.createCollection("bhs_form_templates")
	+ db.createCollection("bhs_form_response")

<!-- running -->
- sudo node bhs-admin-server.js
<!-- https://www.npmjs.com/package/forever -->
- sudo npm install forever -g
- sudo forever start bhs-admin-server.js
- sudo forever list